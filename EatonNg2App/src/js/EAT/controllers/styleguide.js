(function() {
	/**
	 * @exports EAT.controllers.styleguide
	 * @requires HBS
	 * @requires prettyPrint
	 */
	var module = {};

	module.init = function() {
		module.navSmoothScroll();
		module.toggleSwitchSetup();
	};

	module.navSmoothScroll = function(){
		$(document).on('click', 'a.styleguide-nav__link', function(event){
			event.preventDefault();

			$('html, body').animate({
				scrollTop: $( $.attr(this, 'href') ).offset().top
			}, 500);
		});
	};

	module.landingPage = function(){
		//$('.dropdown-toggle').dropdown();
	};

	module.checkToggleState = function(){

	};

	module.toggleSwitchSetup = function(){
		var $toggleSwitch = $('.switch-toggle');
		$toggleSwitch.each(function(){
			$(this).find('input:checked').next('label').addClass('active-label');
		});

		$toggleSwitch.on('click','label', function(e){
			console.log($(e.target));
			console.log($(this).prev('input'));
			$(this).parents('.switch-toggle').find('label').removeClass('active-label');
			$(this).parents('.switch-toggle').find('input').removeAttr('checked');
			$(this).prev('input').attr('checked', true);
			$(this).addClass('active-label');
		});
	};


	HBS.namespace('EAT.controllers.styleguide', module);
}());
