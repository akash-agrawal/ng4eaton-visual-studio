import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { AssetService } from './asset.service';
import { AssetModel } from './asset';
import { CreateAssetModalComponent } from '../common-services/modals/create-asset-modal/create-asset-modal.component';

@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.css'],
  providers: [AssetService]
})
export class AssetComponent implements OnInit {
    @ViewChild('createAssetModal') createAssetModal: CreateAssetModalComponent;

    private model: AssetModel;
    public rows = [];
    public columns;
    public selected = [];

    constructor(private assetService: AssetService, private router: Router) {
        this.model = new AssetModel();
    }

    ngOnInit(): void {
        this.model.autoHeight = window.innerHeight;
        this.model.config = {
            "initialLoad": true,
            "order": "-createdDate",
            "pageNo": 1,
            "pageSize": 10,
            "searchDefaultLocation": true
        }
        this.getAssetList(this.model.config);
    }

    getAssetList(config) {
        this.assetService.getAllAssets(config)
            .subscribe(
            data => {
                this.model.assetList = data;
                console.log("this.model.assetList", this.model.assetList);
                this.rows = this.model.assetList.assetList;
                //this.columns = [
                //    { prop: 'assetBarcode' },
                //    { name: 'Asset Description' },
                //    { name: 'Tenant Id' }
                //];
            },
            error => {
                console.log(error);
            });
    }

    autoSearch(value) {
        if (value.length > 3) {
            this.assetService.search(value)
                .subscribe(
                data => {
                    //console.log(data);
                    this.model.assetList = data;
                    //console.log("this.model.assetList", this.model.assetList);
                },
                error => {
                    alert("Something went wrong!!");
                },
                () => {
                });
        }
    }

    onSelect({ selected }) {
        console.log('Select Event', selected, this.selected);

        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    onActivate(event) {
        //console.log('Activate Event', event);
    }

    add() {
        this.selected.push(this.rows[1], this.rows[3]);
    }

    update() {
        this.selected = [this.rows[1], this.rows[3]];
    }

    remove() {
        this.selected = [];
    }

    getShowingCount(recordsPerPage, currentPage) {
        this.model.showingCountFrom = ((currentPage * recordsPerPage) - (recordsPerPage - 1));
    }

    pagesDropdown(totalPages) {
        return new Array(totalPages);
    }

    pageChange(currentPage) {
        console.log("currentPage", currentPage);
        this.getShowingCount(this.model.recordsPerPage, currentPage);
        this.model.config.pageNo = currentPage;
        console.log("config", this.model.config);
        this.getAssetList(this.model.config);
    }

    changeRowsPerPage(value) {
        console.log(value);
        this.model.config.pageSize = value;
        console.log("config", this.model.config);
        this.getAssetList(this.model.config);
    }

    navigateToComponent(asset, event) {
        this.model.contextMenuAsset = asset;
        if (event.type == 'mouseup') {
            if (event.which === 3) {
                this.model.eatonContextMenu = { 'display': 'block', 'left': (event.clientX - 250) + 'px', 'top': event.clientY + 'px' };
                return false;
            } else if (event.which === 1) {
                localStorage.setItem('_id', asset._id);
                this.router.navigate(['/overview']);
            }
        } else if (event.type == 'click') {
            console.log(event.target.checked);
        }
    }

    navigateToComponentContextMenu() {
        localStorage.setItem('_id', this.model.contextMenuAsset._id);
        this.router.navigate(['/pages/overview/']);
    }

    cloneAssetContextMenu() {
        console.log("Clone ASset");
    }

    //function to hide the Eaton Context Menu
    closeEatonContextMenu() {
        this.model.eatonContextMenu = { 'display': 'none' };
    }
}
