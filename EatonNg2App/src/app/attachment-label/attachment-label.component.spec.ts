import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmentLabelComponent } from './attachment-label.component';

describe('AttachmentLabelComponent', () => {
  let component: AttachmentLabelComponent;
  let fixture: ComponentFixture<AttachmentLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachmentLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
