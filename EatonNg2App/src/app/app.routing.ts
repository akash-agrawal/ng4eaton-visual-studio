import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AssetComponent } from './asset/asset.component';
import { AssetOverviewComponent } from './asset-overview/asset-overview.component'
import { SetupComponent } from './setup/setup.component';
import { MyLocationComponent } from './my-location/my-location.component';
import { CategoryManagementComponent } from './category-management/category-management.component';
import { FormFieldsComponent } from './form-fields/form-fields.component';
import { FormsComponent } from './forms/forms.component';
import { UsersComponent } from './users/users.component';
import { AttachmentLabelComponent } from './attachment-label/attachment-label.component';
import { LayoutComponent } from './common-services/layout/layout.component';

import { AuthGuard } from './common-services/authGuard/auth-guard.service';

const APP_ROUTES: Routes = [
    {
        path: '', component: LoginComponent,
        children: [
            {
                path: '', component: AppComponent
            }
        ]
    },
    {
        path: 'dashboard', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', component: DashboardComponent,
            }
        ]
    },
    {
        path: 'asset', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', component: AssetComponent,
            }
        ]
    },
    {
        path: 'overview', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', component: AssetOverviewComponent,
            }
        ]
    },
    {
        path: 'setup', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', component: SetupComponent,
            }
        ]
    },
    {
        path: 'my-location', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', component: MyLocationComponent,
            }
        ]
    },
    {
        path: 'category-management', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', component: CategoryManagementComponent,
            }
        ]
    },
    {
        path: 'form-fields', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', component: FormFieldsComponent,
            }
        ]
    },
    {
        path: 'form', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', component: FormsComponent,
            }
        ]
    },
    {
        path: 'users', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', component: UsersComponent,
            }
        ]
    },
    {
        path: 'attachment-label', component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', component: AttachmentLabelComponent,
            }
        ]
    }
];

export const routing = RouterModule.forRoot(APP_ROUTES);
