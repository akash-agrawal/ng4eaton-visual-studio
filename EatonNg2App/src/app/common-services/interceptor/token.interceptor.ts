import { Component, Injectable, ViewChild } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AuthGuard } from '../authGuard/auth-guard.service';
import { Observable } from 'rxjs/Observable';

import { ErrorModalComponent } from '../modals/error-modal/error-modal.component';
import { NotificationComponent } from '../modals/notification/notification.component';

@Injectable()

export class TokenInterceptor implements HttpInterceptor {
    @ViewChild('errorModal') errorModal: ErrorModalComponent;

    constructor(public auth: AuthGuard, public notify: NotificationComponent) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: {
                'Authorization': `Bearer ${this.auth.getToken()}`,
                'Content-Type': 'application/json'
            }
        });
        return next.handle(request).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                this.notify.showSuccess("Success", "Data fetch successful");
            }
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                this.notify.showError(err.status + ': ' + err.name, JSON.parse(err.error).Message);
                console.log("error", err);
                //this.errorModal.show();
                //if (err.status === 405) {
                //    // redirect to the login route
                //    // or show a modal
                //    alert("something went wrong from token interceptor");
                //    this.auth.collectFailedRequest(request);
                //}
            }
        });;
    }
}
