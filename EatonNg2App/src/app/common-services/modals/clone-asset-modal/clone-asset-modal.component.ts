import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-clone-asset-modal',
    templateUrl: './clone-asset-modal.component.html',
    styleUrls: ['./clone-asset-modal.component.css']
})
export class CloneAssetModalComponent implements OnInit {
    @ViewChild('cloneAssetModal') public cloneAssetModal: ModalDirective;

    constructor() { }

    ngOnInit() {
    }

    show() {
        this.cloneAssetModal.show();
    }

    hide() {
        this.cloneAssetModal.hide();
    }
}
