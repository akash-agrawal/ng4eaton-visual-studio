import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-add-location-modal',
    templateUrl: './add-location-modal.component.html',
    styleUrls: ['./add-location-modal.component.css']
})
export class AddLocationModalComponent implements OnInit {
    @ViewChild('addLocationModal') public addLocationModal: ModalDirective;

    constructor() { }

    ngOnInit() {
    }

    show() {
        this.addLocationModal.show();
    }

    hide() {
        this.addLocationModal.hide();
    }

}
