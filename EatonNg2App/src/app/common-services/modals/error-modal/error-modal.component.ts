import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-error-modal',
    templateUrl: './error-modal.component.html',
    styleUrls: ['./error-modal.component.css']
})
export class ErrorModalComponent {
    @ViewChild('errorModal') public errorModal: ModalDirective;

    constructor() { }

    show() {
        this.errorModal.show();
    }
}
