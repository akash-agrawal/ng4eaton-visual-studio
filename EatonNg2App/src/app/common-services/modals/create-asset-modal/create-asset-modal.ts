export class CreateAssetModel {
    public customerList: any;
    public quantityList: any;
    public locationList: any;
    public marketSegment: any;
    public hoseList: any;
    public componentPartbyIdList: any;
    public componentPartImage: any;
    public componentPartForList: any;
    public showErrorMessage: boolean;
    public selectedHose: string;
    public fitting1Detail: any;
    public selectedFitting1: any;
    public fitting1Image: any;
    public selectedFitting1Id: string;
    public selectedFitting2: any;
    public selectedFitting2Id: string;
    public fitting2Detail: any;
    public fitting2Image: any;

    public customer: any;

    constructor() {
        this.customerList = [];
        this.quantityList = [];
        this.locationList = [];
        this.hoseList = [];
        this.componentPartbyIdList = [];
        this.componentPartForList = [];
        this.showErrorMessage = false;
        this.selectedHose = null;
        this.fitting1Detail = [];
        this.selectedFitting1 = null;
        this.selectedFitting2 = null;
        this.fitting2Detail = [];
    }
}
