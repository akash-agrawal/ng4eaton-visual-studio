import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../../../app.global';

@Injectable()
export class CreateAssetModalService {

    constructor(private http: HttpClient) { }

    getCustomers(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'customer/list/' + tenantId);
    }

    getLocations(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'location/lookup/list/' + tenantId);
    }

    getMarketSegments(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'getCustomerMarketSegmentApplication/' + tenantId);
    }

    getComponentPart(partType, isAll) {
        const obj = {
            'ComponentPartType': partType,
            'IsAll': isAll
        }
        const body = JSON.stringify(obj);
        return this.http.post(myGlobals.baseUrl + 'componentPart/list', body);
    }

    getComponentPartbyId(id) {
        const obj = {
            'ComponentPartId': id
        }
        const body = JSON.stringify(obj);
        return this.http.post(myGlobals.baseUrl + 'componentPart/GetComponentPartById', body);
    }

    getComponentImage(componentObj) {
        const obj = {
            'azureBlobUID': componentObj.primaryImage,
            'containerName': componentObj.createdByTID,
            'convertToBase64': true,
            'fileName':''
        }
        const body = JSON.stringify(obj);
        return this.http.post(myGlobals.baseUrl + 'image/download', body);
    }

    getComponentPartIdForList(id) {
        const obj = {
            'ComponentPartId': id
        }
        const body = JSON.stringify(obj);
        return this.http.post(myGlobals.baseUrl + 'componentPart/GetCompatiblePartDetailsForList', body);
    }
}
