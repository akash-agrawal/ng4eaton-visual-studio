import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../../../app.global';

@Injectable()
export class ReplaceAssetModalService {

    constructor(private http: HttpClient) { }

    getAssets() {
        const obj = {
            'initialLoad': true,
            'order': "-createdDate",
            'pageNo': 1,
            'pageSize': 10,
            'searchDefaultLocation': true
        }
        const body = JSON.stringify(obj);
        return this.http.post(myGlobals.baseUrl + 'asset/search', body);
    }

}
