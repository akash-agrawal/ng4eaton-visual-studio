import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { ReplaceAssetModalService } from './replace-asset-modal.service';
//import { ReplaceAssetModel } from './replace-asset-modal';

@Component({
    selector: 'app-replace-asset-modal',
    templateUrl: './replace-asset-modal.component.html',
    styleUrls: ['./replace-asset-modal.component.css'],
    providers: [ReplaceAssetModalService]
})
export class ReplaceAssetModalComponent implements OnInit {
    @ViewChild('replaceAssetModal') public replaceAssetModal: ModalDirective;

    constructor(private replaceAssetModalService: ReplaceAssetModalService) { }

    ngOnInit() {
    }

    show() {
        this.replaceAssetModal.show();
        this.getAssets();
    }

    hide() {
        this.replaceAssetModal.hide();
    }

    getAssets() {
        this.replaceAssetModalService.getAssets()
            .subscribe(
            data => {
                console.log("data", data);
            },
            error => {
                console.log(error);
            });
    }
}
