import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'toastr-ng2';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

    constructor(private toastrService: ToastrService) { }

    ngOnInit() {
    }

    showSuccess(message, heading) {
        this.toastrService.success(heading, message);
    }

    showError(message, heading) {
        this.toastrService.error(heading, message);
    }

    showInfo(message, heading) {
        this.toastrService.info(heading, message);
    }

    showWarning(message, heading) {
        this.toastrService.warning(heading, message);
    }

}
