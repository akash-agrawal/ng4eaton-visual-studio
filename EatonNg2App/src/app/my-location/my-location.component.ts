import { Component, OnInit, ViewChild } from '@angular/core';

import { MyLocationService } from './my-location.service';
import { MyLocationModel } from './my-location';
import { AddLocationModalComponent } from '../common-services/modals/add-location-modal/add-location-modal.component';

@Component({
    selector: 'app-my-location',
    templateUrl: './my-location.component.html',
    styleUrls: ['./my-location.component.css'],
    providers: [MyLocationService]
})
export class MyLocationComponent implements OnInit {
    @ViewChild('addLocationModal') public addLocationModal: AddLocationModalComponent;

    private model: MyLocationModel;

    constructor(private myLocationService: MyLocationService) {
        this.model = new MyLocationModel();
    }

    ngOnInit() {
        this.getLocation();
    }

    addLocation() {
        this.addLocationModal.show();
    }

    getLocation() {
        const tenantId = JSON.parse(localStorage.getItem('profile')).tenantId;

        this.myLocationService.getLocation(tenantId)
            .subscribe(
            data => {
                console.log("data", data);
                this.model.locationList = data;
            },
            error => {
                console.log(error);
            },
            () => {
                //pre-select the first location to show the details
                this.selectLocEdit(this.model.locationList[0]._id);
            }
            );
    }

    selectLocEdit(id) {
        const locationList = this.model.locationList;
        this.model.locObj = locationList.filter(x => x._id == id).map(x => x)[0];
        console.log(this.model.locObj);
    }

    selectChildLocEdit(id) {
        const locationList = this.model.locationList;
        this.model.locObj = locationList.filter(x => x.subLocations.length !== 0).map(x => { let childArray = Object.assign({}, x); return childArray.subLocations.filter(y => y._id == id).map(y => y)[0] })[0];
        console.log(this.model.locObj);
    }

}
