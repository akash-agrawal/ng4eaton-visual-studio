import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';

import * as myGlobals from '../app.global';

@Injectable()
export class LoginService {

    constructor(private http: Http) { }

    authenticateUser(username, password) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let data = {
            "client_id": "fra02s7xtdIVSLyfNgkieoMQC6B7mwmf",
            "connection": "Username-Password-Authentication",
            "grant_type": "password",
            "password": password,
            "scope": "openid email app_metadata user_id",
            "username": username
        }
        const body = JSON.stringify(data);
        return this.http.post(myGlobals.authUrl + 'oauth/ro', body, {
            headers: headers
        })
            .map((response: Response) => response.json());
    }

    tokenInfo(idToken) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let data = {
            "id_token": idToken
        }
        const body = JSON.stringify(data);
        return this.http.post(myGlobals.authUrl + 'tokeninfo', body, {
            headers: headers
        })
            .map((response: Response) => response.json());
    }

    getAuth0(identity) {
        const headers = new Headers();
        headers.append('Authorization', "bearer " + localStorage.getItem('token'));
        return this.http.get(myGlobals.baseUrl + 'user/auth0/' + identity, {
            headers: headers
        })
            .map((response: Response) => response.json());
    }
}
