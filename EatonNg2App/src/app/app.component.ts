import { Component, ViewChild, OnInit, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

import { AppModel } from './app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    private model: AppModel;

    @ViewChild('myModal') public myModal: ModalDirective;

    constructor(private router: Router) {
        this.model = new AppModel();
    }

    ngOnInit() {
        this.checkAuth();
    }

    checkAuth() {
        if (localStorage.getItem('token')) {
            this.model.isLoggedIn = true;
        } else {
            this.model.isLoggedIn = false;
        }
    }

    showModal() {
        this.myModal.show();
    }
}
