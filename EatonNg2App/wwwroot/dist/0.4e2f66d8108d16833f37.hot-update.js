webpackHotUpdateac__name_(0,{

/***/ 857:
/***/ (function(module, exports) {

module.exports = "<app-header [hidden]=\"!model.isLoggedIn\"></app-header>\r\n<div id=\"wrapper\">\r\n  <div id=\"main-wrapper\">\r\n    <span></span>\r\n    <app-sidebar [hidden]=\"!model.isLoggedIn\"></app-sidebar>\r\n    <div id=\"main\" class=\"p-0\">\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ })

})
//# sourceMappingURL=main.map