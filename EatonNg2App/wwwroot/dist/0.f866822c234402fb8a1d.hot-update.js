webpackHotUpdateac__name_(0,{

/***/ 856:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app__ = __webpack_require__(881);




var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
        this.model = new __WEBPACK_IMPORTED_MODULE_3__app__["a" /* AppModel */]();
    }
    AppComponent.prototype.ngOnInit = function () {
        console.log(localStorage.getItem('token'));
        if (localStorage.getItem('token')) {
            this.model.isLoggedIn = true;
        }
        else {
            this.model.isLoggedIn = false;
        }
    };
    AppComponent.prototype.showModal = function () {
        this.myModal.show();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myModal'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["a" /* ModalDirective */])
    ], AppComponent.prototype, "myModal", void 0);
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(857),
            styles: [__webpack_require__(858)]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ })

})
//# sourceMappingURL=main.map