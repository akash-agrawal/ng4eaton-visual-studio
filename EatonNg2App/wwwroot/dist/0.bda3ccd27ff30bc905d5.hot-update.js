webpackHotUpdateac__name_(0,{

/***/ 842:
/***/ (function(module, exports) {

module.exports = "<div class=\"login-bg\">\r\n  <section class=\"login\">\r\n    <div class=\"login__body\">\r\n      <div class=\"login__head\">\r\n        <a href=\"#\" class=\"login__brand\">\r\n          <img src=\"../src/images/eatonlogo.svg\" width=\"180\" height=\"60\">\r\n        </a>\r\n      </div>\r\n      <div class=\"login__content\">\r\n        <form name=\"form\" (ngSubmit)=\"f.form.valid && loginUser()\" #f=\"ngForm\" novalidate>\r\n          <div class=\"form-group form-group--withbutton mbn\">\r\n            <label for=\"\" class=\"block\">Email Address</label>\r\n            <div class=\"input-group\">\r\n              <div class=\"input-group-addon input-group-addon--blue\"><i class=\"icon-mail\"></i></div>\r\n              <input type=\"email\" class=\"form-control\" id=\"Email Field\" name=\"useremail\" placeholder=\"Enter Email Address\" [(ngModel)]=\"model.useremail\" #email=\"ngModel\">\r\n            </div>\r\n\r\n            <label for=\"\" class=\"block mtm\">Password</label>\r\n            <div class=\"input-group\">\r\n              <div class=\"input-group-addon input-group-addon--blue\"><i class=\"icon-locked\"></i></div>\r\n              <input type=\"password\" class=\"form-control\" id=\"passwordField\" name=\"password\" placeholder=\"Enter Password\" [(ngModel)]=\"model.password\" #password=\"ngModel\">\r\n            </div>\r\n\r\n            <p class=\"forgot-password pvm\"><a href=\"#\">Forgot your password? Tes1212sasast</a></p>\r\n\r\n            <div class=\"login__buttonwrap\">\r\n              <button class=\"btn btn-primary\">Login</button>\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>\r\n"

/***/ })

})
//# sourceMappingURL=main.map