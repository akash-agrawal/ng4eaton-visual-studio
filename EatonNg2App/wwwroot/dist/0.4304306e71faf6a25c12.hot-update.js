webpackHotUpdateac__name_(0,{

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_service__ = __webpack_require__(590);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login__ = __webpack_require__(841);




var LoginComponent = /** @class */ (function () {
    function LoginComponent(loginService, router) {
        this.loginService = loginService;
        this.router = router;
        this.model = new __WEBPACK_IMPORTED_MODULE_3__login__["a" /* LoginModel */]();
    }
    LoginComponent.prototype.ngOnInit = function () {
        if (localStorage.getItem('token')) {
            this.model.isLoggedIn = true;
        }
        else {
            this.model.isLoggedIn = false;
        }
    };
    LoginComponent.prototype.loginUser = function () {
        var _this = this;
        this.model.loginSuccess = true;
        this.loginService.authenticateUser(this.model.useremail, this.model.password)
            .subscribe(function (data) {
            console.log("1", data);
            _this.model.ro = data;
            localStorage.setItem('token', data.id_token);
        }, function (error) {
            alert("Something went wrong!!");
        }, function () {
            _this.getTokenInfo(_this.model.ro.id_token);
        });
    };
    LoginComponent.prototype.getTokenInfo = function (idToken) {
        var _this = this;
        this.loginService.tokenInfo(idToken)
            .subscribe(function (data) {
            console.log("2", data);
            _this.model.tokenInfo = data;
            localStorage.setItem('profile', JSON.stringify(data));
        }, function (error) {
            alert("Something went wrong!!");
        }, function () {
            _this.getAuth0(_this.model.tokenInfo.identities[0].provider + '|' + _this.model.tokenInfo.identities[0].user_id);
        });
    };
    LoginComponent.prototype.getAuth0 = function (identity) {
        var _this = this;
        this.loginService.getAuth0(identity)
            .subscribe(function (data) {
            console.log("3", data);
            _this.model.auth0 = data;
        }, function (error) {
            alert("Something went wrong!!");
        }, function () {
            _this.model.loginSuccess = false;
            _this.router.navigate(['/dashboard']);
        });
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'login',
            template: __webpack_require__(842),
            styles: [__webpack_require__(843)],
            providers: [__WEBPACK_IMPORTED_MODULE_2__login_service__["a" /* LoginService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__login_service__["a" /* LoginService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_services_modals_create_asset_modal_create_asset_modal_component__ = __webpack_require__(869);


var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('createAssetModal'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__common_services_modals_create_asset_modal_create_asset_modal_component__["a" /* CreateAssetModalComponent */])
    ], DashboardComponent.prototype, "createAssetModal", void 0);
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(845),
            styles: [__webpack_require__(846)]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssetComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__asset_service__ = __webpack_require__(848);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__asset__ = __webpack_require__(849);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_services_modals_create_asset_modal_create_asset_modal_component__ = __webpack_require__(869);





var AssetComponent = /** @class */ (function () {
    function AssetComponent(assetService, router) {
        this.assetService = assetService;
        this.router = router;
        this.rows = [];
        this.selected = [];
        this.model = new __WEBPACK_IMPORTED_MODULE_3__asset__["a" /* AssetModel */]();
    }
    AssetComponent.prototype.ngOnInit = function () {
        this.model.autoHeight = window.innerHeight;
        this.model.config = {
            "initialLoad": true,
            "order": "-createdDate",
            "pageNo": 1,
            "pageSize": 10,
            "searchDefaultLocation": true
        };
        this.getAssetList(this.model.config);
    };
    AssetComponent.prototype.getAssetList = function (config) {
        var _this = this;
        this.assetService.getAllAssets(config)
            .subscribe(function (data) {
            _this.model.assetList = data;
            console.log("this.model.assetList", _this.model.assetList);
            _this.rows = _this.model.assetList.assetList;
            //this.columns = [
            //    { prop: 'assetBarcode' },
            //    { name: 'Asset Description' },
            //    { name: 'Tenant Id' }
            //];
        }, function (error) {
            console.log(error);
        });
    };
    AssetComponent.prototype.autoSearch = function (value) {
        var _this = this;
        if (value.length > 3) {
            this.assetService.search(value)
                .subscribe(function (data) {
                //console.log(data);
                _this.model.assetList = data;
                //console.log("this.model.assetList", this.model.assetList);
            }, function (error) {
                alert("Something went wrong!!");
            }, function () {
            });
        }
    };
    AssetComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        console.log('Select Event', selected, this.selected);
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    AssetComponent.prototype.onActivate = function (event) {
        //console.log('Activate Event', event);
    };
    AssetComponent.prototype.add = function () {
        this.selected.push(this.rows[1], this.rows[3]);
    };
    AssetComponent.prototype.update = function () {
        this.selected = [this.rows[1], this.rows[3]];
    };
    AssetComponent.prototype.remove = function () {
        this.selected = [];
    };
    AssetComponent.prototype.getShowingCount = function (recordsPerPage, currentPage) {
        this.model.showingCountFrom = ((currentPage * recordsPerPage) - (recordsPerPage - 1));
    };
    AssetComponent.prototype.pagesDropdown = function (totalPages) {
        return new Array(totalPages);
    };
    AssetComponent.prototype.pageChange = function (currentPage) {
        console.log("currentPage", currentPage);
        this.getShowingCount(this.model.recordsPerPage, currentPage);
        this.model.config.pageNo = currentPage;
        console.log("config", this.model.config);
        this.getAssetList(this.model.config);
    };
    AssetComponent.prototype.changeRowsPerPage = function (value) {
        console.log(value);
        this.model.config.pageSize = value;
        console.log("config", this.model.config);
        this.getAssetList(this.model.config);
    };
    AssetComponent.prototype.navigateToComponent = function (asset, event) {
        this.model.contextMenuAsset = asset;
        if (event.type == 'mouseup') {
            if (event.which === 3) {
                this.model.eatonContextMenu = { 'display': 'block', 'left': (event.clientX - 250) + 'px', 'top': event.clientY + 'px' };
                return false;
            }
            else if (event.which === 1) {
                localStorage.setItem('_id', asset._id);
                this.router.navigate(['/overview']);
            }
        }
        else if (event.type == 'click') {
            console.log(event.target.checked);
        }
    };
    AssetComponent.prototype.navigateToComponentContextMenu = function () {
        localStorage.setItem('_id', this.model.contextMenuAsset._id);
        this.router.navigate(['/pages/overview/']);
    };
    AssetComponent.prototype.cloneAssetContextMenu = function () {
        console.log("Clone ASset");
    };
    //function to hide the Eaton Context Menu
    AssetComponent.prototype.closeEatonContextMenu = function () {
        this.model.eatonContextMenu = { 'display': 'none' };
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('createAssetModal'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4__common_services_modals_create_asset_modal_create_asset_modal_component__["a" /* CreateAssetModalComponent */])
    ], AssetComponent.prototype, "createAssetModal", void 0);
    AssetComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-asset',
            template: __webpack_require__(850),
            styles: [__webpack_require__(851)],
            providers: [__WEBPACK_IMPORTED_MODULE_2__asset_service__["a" /* AssetService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__asset_service__["a" /* AssetService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]])
    ], AssetComponent);
    return AssetComponent;
}());



/***/ }),

/***/ 528:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(868);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__swimlane_ngx_datatable__ = __webpack_require__(588);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_routing__ = __webpack_require__(589);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__common_services_authGuard_auth_guard_service__ = __webpack_require__(870);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__common_services_interceptor_token_interceptor__ = __webpack_require__(877);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_component__ = __webpack_require__(856);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__login_login_component__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__dashboard_dashboard_component__ = __webpack_require__(336);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__asset_asset_component__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__asset_overview_asset_overview_component__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__layout_header_header_component__ = __webpack_require__(860);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__layout_sidebar_sidebar_component__ = __webpack_require__(864);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__common_services_modals_error_modal_error_modal_component__ = __webpack_require__(871);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__common_services_modals_create_asset_modal_create_asset_modal_component__ = __webpack_require__(869);






















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_12__login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_13__dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_14__asset_asset_component__["a" /* AssetComponent */],
                __WEBPACK_IMPORTED_MODULE_15__asset_overview_asset_overview_component__["a" /* AssetOverviewComponent */],
                __WEBPACK_IMPORTED_MODULE_16__layout_header_header_component__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_17__layout_sidebar_sidebar_component__["a" /* SidebarComponent */],
                __WEBPACK_IMPORTED_MODULE_18__common_services_modals_error_modal_error_modal_component__["a" /* ErrorModalComponent */],
                __WEBPACK_IMPORTED_MODULE_19__common_services_modals_create_asset_modal_create_asset_modal_component__["a" /* CreateAssetModalComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_8__app_routing__["a" /* routing */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["b" /* ButtonsModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["d" /* PaginationModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["c" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["a" /* AlertModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["e" /* TooltipModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_7__swimlane_ngx_datatable__["NgxDatatableModule"]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_9__common_services_authGuard_auth_guard_service__["a" /* AuthGuard */],
                {
                    provide: __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HTTP_INTERCEPTORS */],
                    useClass: __WEBPACK_IMPORTED_MODULE_10__common_services_interceptor_token_interceptor__["a" /* TokenInterceptor */],
                    multi: true
                }
            ],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_18__common_services_modals_error_modal_error_modal_component__["a" /* ErrorModalComponent */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ 589:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__login_login_component__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard_component__ = __webpack_require__(336);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__asset_asset_component__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__asset_overview_asset_overview_component__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_services_authGuard_auth_guard_service__ = __webpack_require__(870);






var APP_ROUTES = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_1__login_login_component__["a" /* LoginComponent */] },
    { path: 'dashboard', component: __WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard_component__["a" /* DashboardComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_5__common_services_authGuard_auth_guard_service__["a" /* AuthGuard */]] },
    { path: 'asset', component: __WEBPACK_IMPORTED_MODULE_3__asset_asset_component__["a" /* AssetComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_5__common_services_authGuard_auth_guard_service__["a" /* AuthGuard */]] },
    { path: 'overview', component: __WEBPACK_IMPORTED_MODULE_4__asset_overview_asset_overview_component__["a" /* AssetOverviewComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_5__common_services_authGuard_auth_guard_service__["a" /* AuthGuard */]] },
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forRoot(APP_ROUTES);


/***/ }),

/***/ 841:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginModel; });
var LoginModel = /** @class */ (function () {
    function LoginModel() {
        this.ro = [];
        this.tokenInfo = [];
        this.auth0 = [];
        this.loginSuccess = false;
        this.isLoggedIn = false;
    }
    return LoginModel;
}());



/***/ }),

/***/ 842:
/***/ (function(module, exports) {

module.exports = "<div class=\"login-bg\">\r\n  <section class=\"login\">\r\n    <div class=\"login__body\">\r\n      <div class=\"login__head\">\r\n        <a href=\"#\" class=\"login__brand\">\r\n          <img src=\"images/eatonlogo.svg\" width=\"180\" height=\"60\">\r\n        </a>\r\n      </div>\r\n      <div class=\"login__content\">\r\n        <form name=\"form\" (ngSubmit)=\"f.form.valid && loginUser()\" #f=\"ngForm\" novalidate>\r\n          <div class=\"form-group form-group--withbutton mbn\">\r\n            <label for=\"\" class=\"block\">Email Address</label>\r\n            <div class=\"input-group\">\r\n              <div class=\"input-group-addon input-group-addon--blue\"><i class=\"icon-mail\"></i></div>\r\n              <input type=\"email\" class=\"form-control\" id=\"Email Field\" name=\"useremail\" placeholder=\"Enter Email Address\" [(ngModel)]=\"model.useremail\" #email=\"ngModel\">\r\n            </div>\r\n\r\n            <label for=\"\" class=\"block mtm\">Password</label>\r\n            <div class=\"input-group\">\r\n              <div class=\"input-group-addon input-group-addon--blue\"><i class=\"icon-locked\"></i></div>\r\n              <input type=\"password\" class=\"form-control\" id=\"passwordField\" name=\"password\" placeholder=\"Enter Password\" [(ngModel)]=\"model.password\" #password=\"ngModel\">\r\n            </div>\r\n\r\n            <p class=\"forgot-password pvm\"><a href=\"#\">Forgot your password?</a></p>\r\n\r\n            <div class=\"login__buttonwrap\">\r\n              <button class=\"btn btn-primary\">Login</button>\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>\r\n"

/***/ }),

/***/ 845:
/***/ (function(module, exports) {

module.exports = "<section class=\"maincontent\">\r\n  <div class=\"maincontent__rightcol\">\r\n    <div class=\"title-bar\">\r\n      <h1 class=\"light man\">Home</h1>\r\n    </div>\r\n    <div class=\"content-main\" role=\"main\">\r\n      <div class=\"homepage-dashboard\">\r\n        <div class=\"homepage-dashboard__col1\">\r\n          <button (click)=\"createAssetModal.show()\" class=\"btn btn-white btn-icon-button\"><i class=\"icon-searchandreplace\"></i> Create Asset</button>\r\n          <a href=\"#\" class=\"btn btn-white btn-icon-button\"><i class=\"icon-createnewasset\"></i> Replace Asset</a>\r\n          <a href=\"#\" class=\"btn btn-white btn-icon-button\"><i class=\"icon-createfromexisting\"></i> Clone Asset</a>\r\n        </div>\r\n        <div class=\"homepage-dashboard__col2\">\r\n          <h2 class=\"h4 mtn\">Eaton Tag Guide</h2>\r\n          <img src=\"../../images/assettagguide.jpg\" alt=\"\" class=\"img-responsive\">\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n<app-create-asset-modal #createAssetModal></app-create-asset-modal>\r\n"

/***/ }),

/***/ 848:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssetService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(868);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_global__ = __webpack_require__(335);




var AssetService = /** @class */ (function () {
    function AssetService(http) {
        this.http = http;
    }
    //Original request before implementing Interceptor
    //getAllAssets(config) {
    //    const headers = new Headers();
    //    headers.append('Content-Type', 'application/json');
    //    headers.append('Authorization', "bearer " + localStorage.getItem('token'));
    //    const body = JSON.stringify(config);
    //    return this.http.post(myGlobals.baseUrl + 'asset/search', body);
    //    .map((response: Response) => response.json());
    //}
    AssetService.prototype.getAllAssets = function (config) {
        var body = JSON.stringify(config);
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__app_global__["b" /* baseUrl */] + 'asset/search', body);
    };
    AssetService.prototype.search = function (value) {
        var config = {
            "assetNumber": value,
            "serialNumber": "",
            "description": "",
            "isLegacyCategory": null,
            "labelsCategories": null,
            "assetLocationIds": null,
            "ownerId": null,
            "chipId": "",
            "advanceSearch": true,
            "createdFromDate": "",
            "createdToDate": "",
            "expiryFromDate": "",
            "expiryToDate": "",
            "inServiceFromDate": "",
            "inServiceToDate": "",
            "outServiceFromDate": "",
            "outServiceToDate": "",
            "lastCompltedWorkFromDate": "",
            "lastCompltedWorkToDate": "",
            "nextFillDateFrom": "",
            "nextFillDateTo": "",
            "filterOn": true,
            "isQuickSearchOn": false,
            "pageNo": 1,
            "pageSize": 10,
            "order": "-createdDate",
            "listOn": false,
            "listOf": null,
            "initialLoad": true,
            "skipPaging": false
        };
        var body = JSON.stringify(config);
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__app_global__["b" /* baseUrl */] + 'asset/search', body)
            .map(function (response) { return response.json(); });
    };
    AssetService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], AssetService);
    return AssetService;
}());



/***/ }),

/***/ 850:
/***/ (function(module, exports) {

module.exports = "<section class=\"maincontent\">\r\n  <div class=\"maincontent__rightcol\">\r\n    <div class=\"title-bar\">\r\n      <h1 class=\"light man\">Asset</h1><span class=\"align-btn-right\"><button class=\"btn btn-white btn-sm\" (click)=\"createAssetModal.show()\">Create Asset</button></span>\r\n    </div>\r\n    <div class=\"content-main\" role=\"main\" style=\"padding:0;\">\r\n      <!--<table class=\"table table-striped table-bordered\">\r\n        <thead>\r\n          <tr>\r\n            <th>\r\n              <div class=\"checkbox\">\r\n                <input class=\"eaton-checkbox\" type=\"checkbox\" name=\"layout\" id=\"0\">\r\n                <label for=\"0\"></label>\r\n              </div>\r\n            </th>\r\n            <th>UniqID</th>\r\n            <th>Owner</th>\r\n            <th>Location</th>\r\n            <th>Category</th>\r\n            <th>Description</th>\r\n            <th>Created Date</th>\r\n            <th>Status</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let asset of model.assetList.assetList\" style=\"cursor: pointer;\">\r\n            <td>\r\n              <div class=\"checkbox\">\r\n                <input class=\"eaton-checkbox\" type=\"checkbox\" name=\"assetCheckbox\" id=\"{{asset._id}}\" (click)=\"navigateToComponent(asset, $event)\">\r\n                <label for=\"{{asset._id}}\"></label>\r\n              </div>\r\n            </td>\r\n            <td (mouseup)=\"navigateToComponent(asset, $event)\">{{asset.assetNumber}}</td>\r\n            <td (mouseup)=\"navigateToComponent(asset, $event)\">{{asset.owner.name}}</td>\r\n            <td (mouseup)=\"navigateToComponent(asset, $event)\">{{asset.assetLocation.name}}</td>\r\n            <td (mouseup)=\"navigateToComponent(asset, $event)\">{{asset.category.categoryName}}</td>\r\n            <td (mouseup)=\"navigateToComponent(asset, $event)\">{{asset.assetDescription}}</td>\r\n            <td (mouseup)=\"navigateToComponent(asset, $event)\">{{asset.createdDate | date}}</td>\r\n            <td (mouseup)=\"navigateToComponent(asset, $event)\">{{asset.status}}</td>\r\n          </tr>\r\n        </tbody>\r\n      </table>-->\r\n      <!--<ngx-datatable class=\"material\"\r\n        [rows]=\"rows\"\r\n        [loadingIndicator]=\"loadingIndicator\"\r\n        [columns]=\"columns\"\r\n        [columnMode]=\"'force'\"\r\n        [headerHeight]=\"50\"\r\n        [footerHeight]=\"50\"\r\n        [rowHeight]=\"'auto'\"\r\n        [reorderable]=\"reorderable\" >\r\n      </ngx-datatable>-->\r\n      <ngx-datatable class=\"material\"\r\n                     [rows]=\"rows\"\r\n                     [columnMode]=\"'force'\"\r\n                     [headerHeight]=\"50\"\r\n                     [footerHeight]=\"50\"\r\n                     [rowHeight]=\"'auto'\"\r\n                     [limit]=\"5\"\r\n                     [selected]=\"selected\"\r\n                     [selectionType]=\"'checkbox'\"\r\n                     (activate)=\"onActivate($event)\"\r\n                     (select)='onSelect($event)'>\r\n        <ngx-datatable-column [width]=\"30\" [sortable]=\"false\" [canAutoResize]=\"false\" [draggable]=\"false\" [resizeable]=\"false\">\r\n          <ng-template ngx-datatable-header-template let-value=\"value\" let-allRowsSelected=\"allRowsSelected\" let-selectFn=\"selectFn\">\r\n            <input type=\"checkbox\" [checked]=\"allRowsSelected\" (change)=\"selectFn(!allRowsSelected)\" />\r\n          </ng-template>\r\n          <ng-template ngx-datatable-cell-template let-value=\"value\" let-isSelected=\"isSelected\" let-onCheckboxChangeFn=\"onCheckboxChangeFn\">\r\n            <input type=\"checkbox\" [checked]=\"isSelected\" (change)=\"onCheckboxChangeFn($event)\" />\r\n          </ng-template>\r\n        </ngx-datatable-column>\r\n        <ngx-datatable-column name=\"assetNumber\"></ngx-datatable-column>\r\n        <ngx-datatable-column name=\"owner.name\"></ngx-datatable-column>\r\n        <ngx-datatable-column name=\"assetLocation.name\"></ngx-datatable-column>\r\n        <ngx-datatable-column name=\"category.categoryName\"></ngx-datatable-column>\r\n        <ngx-datatable-column name=\"assetDescription\"></ngx-datatable-column>\r\n        <ngx-datatable-column name=\"createdDate\"></ngx-datatable-column>\r\n        <ngx-datatable-column name=\"status\"></ngx-datatable-column>\r\n      </ngx-datatable>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n\r\n\r\n<!--<div class=\"eatonContextMenu\" [ngStyle]=\"model.eatonContextMenu\" (clickOutside)=\"closeEatonContextMenu()\" oncontextmenu=\"return false\">\r\n  <div class=\"eatonContextMenuLink\" style=\"margin-top:2px;\" (click)=\"navigateToComponentContextMenu()\"><a>Asset Overview</a></div>\r\n  <div class=\"eatonContextMenuLink\"><a>Edit Asset</a></div>\r\n  <div class=\"eatonContextMenuLink\" (click)=\"cloneAssetContextMenu()\"><a>Clone Asset</a></div>\r\n  <div class=\"eatonContextMenuLink\"><a>Replace Asset</a></div>\r\n  <div class=\"eatonContextMenuLink\"><a>Delete Asset</a></div>\r\n</div>-->\r\n\r\n<app-create-asset-modal #createAssetModal></app-create-asset-modal>\r\n"

/***/ }),

/***/ 856:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app__ = __webpack_require__(881);




var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
        this.model = new __WEBPACK_IMPORTED_MODULE_3__app__["a" /* AppModel */]();
    }
    AppComponent.prototype.ngOnInit = function () {
        if (localStorage.getItem('token')) {
            this.model.isLoggedIn = true;
        }
        else {
            this.model.isLoggedIn = false;
        }
    };
    AppComponent.prototype.showModal = function () {
        this.myModal.show();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myModal'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["a" /* ModalDirective */])
    ], AppComponent.prototype, "myModal", void 0);
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(857),
            styles: [__webpack_require__(858)]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ 857:
/***/ (function(module, exports) {

module.exports = "<app-header [hidden]=\"!model.isLoggedIn\"></app-header>\r\n<div id=\"wrapper\">\r\n  <div id=\"main-wrapper\">\r\n    <app-sidebar [hidden]=\"!model.isLoggedIn\"></app-sidebar>\r\n    <div id=\"main\" class=\"p-0\">\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ 861:
/***/ (function(module, exports) {

module.exports = "<!--<section class=\"header-bar\">\r\n  <div class=\"header-bar__leftcol\">\r\n    <div class=\"row pvs\">\r\n      <div class=\"col-sm-12 text-center\">\r\n        <a href=\"#\" class=\"brand\"><img src=\"images/eatonlogo.svg\" width=\"150\" height=\"50\"></a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"header-bar__rightcol\">\r\n    <div class=\"header-bar__account\">\r\n      <div class=\"header-bar__account-info\">\r\n        <div>Welcome, <strong>Eaton Corporation</strong></div>\r\n        <div><a href=\"#\">Account: johnsmith@mail.com (TenantID:000000)</a></div>\r\n      </div>\r\n      <div class=\"header-bar__account-buttons\">\r\n        <div class=\"dropdown\">\r\n          <button href=\"#\" class=\"btn btn-account dropdown-toggle\" type=\"button\" id=\"accountMenu\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"icon-singleuser\"></i> Account</button>\r\n          <ul class=\"dropdown-menu\" aria-labelledby=\"accountMenu\">\r\n            <li><a href=\"#\">Some Menu Item</a></li>\r\n            <li><a href=\"#\">Another Menu Item</a></li>\r\n            <li><a href=\"#\">A Third Menu Item</a></li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>-->\r\n\r\n<div id=\"header\" class=\"navbar navbar-default navbar-fixed-top\">\r\n  <div class=\"navbar-header\">\r\n    <button class=\"navbar-toggle collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">\r\n      <i class=\"icon-reorder\"></i>\r\n    </button>\r\n    <img src=\"./src/images/eatonlogo.svg\" width=\"150\" height=\"50\" />\r\n  </div>\r\n  <nav class=\"collapse navbar-collapse\">\r\n    <ul class=\"nav navbar-nav pull-right nav-account\">\r\n      <li class=\"dropdown\">\r\n        <button href=\"#\" class=\"btn btn-account dropdown-toggle\" type=\"button\" id=\"accountMenu\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"icon-singleuser\"></i> Account</button>\r\n        <ul class=\"dropdown-menu\" aria-labelledby=\"accountMenu\">\r\n          <li><a href=\"#\">Some Menu Item</a></li>\r\n          <li><a href=\"#\">Another Menu Item</a></li>\r\n          <li><a href=\"#\">A Third Menu Item</a></li>\r\n        </ul>\r\n      </li>\r\n    </ul>\r\n  </nav>\r\n</div>\r\n"

/***/ }),

/***/ 865:
/***/ (function(module, exports) {

module.exports = "<!--<section class=\"maincontent\">\r\n  <div class=\"maincontent__leftcol\">\r\n    <div class=\"site-search\">\r\n      <div class=\"search-wrap\">\r\n        <input type=\"search\" class=\"form-control search-control\" placeholder=\"Search asset\">\r\n        <button type=\"submit\"><i class=\"icon-search\"></i></button>\r\n      </div>\r\n      <a href=\"#\" class=\"advanced-search\" data-reveal-id=\"advancedSearch\">Advanced Search</a>\r\n    </div>\r\n    <ul class=\"main-nav\">\r\n      <li class=\"main-nav__item active\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-house\"></i>Home <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-grid\"></i>Dashboard <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-boxopen\"></i>Assets <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-listedit\"></i>Reporting <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-calendarnum\"></i>Basic Form Scheduling <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-people2\"></i>Customers <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-sun\"></i>Setup <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-help\"></i>Help <i class=\"icon-chevronright\"></i></a></li>\r\n    </ul>\r\n  </div>\r\n  <router-outlet></router-outlet>\r\n</section>-->\r\n<div id=\"sidebar-wrapper\">\r\n  <div id=\"sidebar\">\r\n    <div class=\"site-search\">\r\n      <div class=\"search-wrap\">\r\n        <input type=\"search\" class=\"form-control search-control\" placeholder=\"Search asset\">\r\n        <button type=\"submit\"><i class=\"icon-search\"></i></button>\r\n      </div>\r\n      <a href=\"#\" class=\"advanced-search\" data-reveal-id=\"advancedSearch\">Advanced Search</a>\r\n    </div>\r\n    <ul class=\"main-nav\">\r\n      <li class=\"main-nav__item active\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-house\"></i>Home <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a routerLink='/dashboard' class=\"main-nav__link\"><i class=\"icon-grid\"></i>Dashboard <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a routerLink='/asset' class=\"main-nav__link\"><i class=\"icon-boxopen\"></i>Assets <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-listedit\"></i>Reporting <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-calendarnum\"></i>Basic Form Scheduling <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-people2\"></i>Customers <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-sun\"></i>Setup <i class=\"icon-chevronright\"></i></a></li>\r\n      <li class=\"main-nav__item\"><a href=\"#\" class=\"main-nav__link\"><i class=\"icon-help\"></i>Help <i class=\"icon-chevronright\"></i></a></li>\r\n    </ul>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ 868:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export HttpBackend */
/* unused harmony export HttpHandler */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return HttpClient; });
/* unused harmony export HttpHeaders */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HTTP_INTERCEPTORS; });
/* unused harmony export JsonpClientBackend */
/* unused harmony export JsonpInterceptor */
/* unused harmony export HttpClientJsonpModule */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return HttpClientModule; });
/* unused harmony export HttpClientXsrfModule */
/* unused harmony export ɵinterceptingHandler */
/* unused harmony export HttpParams */
/* unused harmony export HttpUrlEncodingCodec */
/* unused harmony export HttpRequest */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return HttpErrorResponse; });
/* unused harmony export HttpEventType */
/* unused harmony export HttpHeaderResponse */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return HttpResponse; });
/* unused harmony export HttpResponseBase */
/* unused harmony export HttpXhrBackend */
/* unused harmony export XhrFactory */
/* unused harmony export HttpXsrfTokenExtractor */
/* unused harmony export ɵa */
/* unused harmony export ɵb */
/* unused harmony export ɵc */
/* unused harmony export ɵd */
/* unused harmony export ɵg */
/* unused harmony export ɵh */
/* unused harmony export ɵe */
/* unused harmony export ɵf */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operator_concatMap__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operator_concatMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_operator_concatMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operator_filter__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_operator_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_operator_map__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_Observable__);

/**
 * @license Angular v4.4.4
 * (c) 2010-2017 Google, Inc. https://angular.io/
 * License: MIT
 */







/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Transforms an `HttpRequest` into a stream of `HttpEvent`s, one of which will likely be a
 * `HttpResponse`.
 *
 * `HttpHandler` is injectable. When injected, the handler instance dispatches requests to the
 * first interceptor in the chain, which dispatches to the second, etc, eventually reaching the
 * `HttpBackend`.
 *
 * In an `HttpInterceptor`, the `HttpHandler` parameter is the next interceptor in the chain.
 *
 * \@experimental
 * @abstract
 */
var HttpHandler = (function () {
    function HttpHandler() {
    }
    /**
     * @abstract
     * @param {?} req
     * @return {?}
     */
    HttpHandler.prototype.handle = function (req) { };
    return HttpHandler;
}());
/**
 * A final `HttpHandler` which will dispatch the request via browser HTTP APIs to a backend.
 *
 * Interceptors sit between the `HttpClient` interface and the `HttpBackend`.
 *
 * When injected, `HttpBackend` dispatches requests directly to the backend, without going
 * through the interceptor chain.
 *
 * \@experimental
 * @abstract
 */
var HttpBackend = (function () {
    function HttpBackend() {
    }
    /**
     * @abstract
     * @param {?} req
     * @return {?}
     */
    HttpBackend.prototype.handle = function (req) { };
    return HttpBackend;
}());
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * A `HttpParameterCodec` that uses `encodeURIComponent` and `decodeURIComponent` to
 * serialize and parse URL parameter keys and values.
 *
 * \@experimental
 */
var HttpUrlEncodingCodec = (function () {
    function HttpUrlEncodingCodec() {
    }
    /**
     * @param {?} k
     * @return {?}
     */
    HttpUrlEncodingCodec.prototype.encodeKey = function (k) { return standardEncoding(k); };
    /**
     * @param {?} v
     * @return {?}
     */
    HttpUrlEncodingCodec.prototype.encodeValue = function (v) { return standardEncoding(v); };
    /**
     * @param {?} k
     * @return {?}
     */
    HttpUrlEncodingCodec.prototype.decodeKey = function (k) { return decodeURIComponent(k); };
    /**
     * @param {?} v
     * @return {?}
     */
    HttpUrlEncodingCodec.prototype.decodeValue = function (v) { return decodeURIComponent(v); };
    return HttpUrlEncodingCodec;
}());
/**
 * @param {?} rawParams
 * @param {?} codec
 * @return {?}
 */
function paramParser(rawParams, codec) {
    var /** @type {?} */ map$$1 = new Map();
    if (rawParams.length > 0) {
        var /** @type {?} */ params = rawParams.split('&');
        params.forEach(function (param) {
            var /** @type {?} */ eqIdx = param.indexOf('=');
            var _a = eqIdx == -1 ?
                [codec.decodeKey(param), ''] :
                [codec.decodeKey(param.slice(0, eqIdx)), codec.decodeValue(param.slice(eqIdx + 1))], key = _a[0], val = _a[1];
            var /** @type {?} */ list = map$$1.get(key) || [];
            list.push(val);
            map$$1.set(key, list);
        });
    }
    return map$$1;
}
/**
 * @param {?} v
 * @return {?}
 */
function standardEncoding(v) {
    return encodeURIComponent(v)
        .replace(/%40/gi, '@')
        .replace(/%3A/gi, ':')
        .replace(/%24/gi, '$')
        .replace(/%2C/gi, ',')
        .replace(/%3B/gi, ';')
        .replace(/%2B/gi, '+')
        .replace(/%3D/gi, '=')
        .replace(/%3F/gi, '?')
        .replace(/%2F/gi, '/');
}
/**
 * An HTTP request/response body that represents serialized parameters,
 * per the MIME type `application/x-www-form-urlencoded`.
 *
 * This class is immuatable - all mutation operations return a new instance.
 *
 * \@experimental
 */
var HttpParams = (function () {
    /**
     * @param {?=} options
     */
    function HttpParams(options) {
        if (options === void 0) { options = {}; }
        this.updates = null;
        this.cloneFrom = null;
        this.encoder = options.encoder || new HttpUrlEncodingCodec();
        this.map = !!options.fromString ? paramParser(options.fromString, this.encoder) : null;
    }
    /**
     * Check whether the body has one or more values for the given parameter name.
     * @param {?} param
     * @return {?}
     */
    HttpParams.prototype.has = function (param) {
        this.init();
        return ((this.map)).has(param);
    };
    /**
     * Get the first value for the given parameter name, or `null` if it's not present.
     * @param {?} param
     * @return {?}
     */
    HttpParams.prototype.get = function (param) {
        this.init();
        var /** @type {?} */ res = ((this.map)).get(param);
        return !!res ? res[0] : null;
    };
    /**
     * Get all values for the given parameter name, or `null` if it's not present.
     * @param {?} param
     * @return {?}
     */
    HttpParams.prototype.getAll = function (param) {
        this.init();
        return ((this.map)).get(param) || null;
    };
    /**
     * Get all the parameter names for this body.
     * @return {?}
     */
    HttpParams.prototype.keys = function () {
        this.init();
        return Array.from(/** @type {?} */ ((this.map)).keys());
    };
    /**
     * Construct a new body with an appended value for the given parameter name.
     * @param {?} param
     * @param {?} value
     * @return {?}
     */
    HttpParams.prototype.append = function (param, value) { return this.clone({ param: param, value: value, op: 'a' }); };
    /**
     * Construct a new body with a new value for the given parameter name.
     * @param {?} param
     * @param {?} value
     * @return {?}
     */
    HttpParams.prototype.set = function (param, value) { return this.clone({ param: param, value: value, op: 's' }); };
    /**
     * Construct a new body with either the given value for the given parameter
     * removed, if a value is given, or all values for the given parameter removed
     * if not.
     * @param {?} param
     * @param {?=} value
     * @return {?}
     */
    HttpParams.prototype.delete = function (param, value) { return this.clone({ param: param, value: value, op: 'd' }); };
    /**
     * Serialize the body to an encoded string, where key-value pairs (separated by `=`) are
     * separated by `&`s.
     * @return {?}
     */
    HttpParams.prototype.toString = function () {
        var _this = this;
        this.init();
        return this.keys()
            .map(function (key) {
            var /** @type {?} */ eKey = _this.encoder.encodeKey(key);
            return ((((_this.map)).get(key))).map(function (value) { return eKey + '=' + _this.encoder.encodeValue(value); })
                .join('&');
        })
            .join('&');
    };
    /**
     * @param {?} update
     * @return {?}
     */
    HttpParams.prototype.clone = function (update) {
        var /** @type {?} */ clone = new HttpParams({ encoder: this.encoder });
        clone.cloneFrom = this.cloneFrom || this;
        clone.updates = (this.updates || []).concat([update]);
        return clone;
    };
    /**
     * @return {?}
     */
    HttpParams.prototype.init = function () {
        var _this = this;
        if (this.map === null) {
            this.map = new Map();
        }
        if (this.cloneFrom !== null) {
            this.cloneFrom.init();
            this.cloneFrom.keys().forEach(function (key) { return ((_this.map)).set(key, /** @type {?} */ ((((((_this.cloneFrom)).map)).get(key)))); }); /** @type {?} */
            ((this.updates)).forEach(function (update) {
                switch (update.op) {
                    case 'a':
                    case 's':
                        var /** @type {?} */ base = (update.op === 'a' ? ((_this.map)).get(update.param) : undefined) || [];
                        base.push(/** @type {?} */ ((update.value))); /** @type {?} */
                        ((_this.map)).set(update.param, base);
                        break;
                    case 'd':
                        if (update.value !== undefined) {
                            var /** @type {?} */ base_1 = ((_this.map)).get(update.param) || [];
                            var /** @type {?} */ idx = base_1.indexOf(update.value);
                            if (idx !== -1) {
                                base_1.splice(idx, 1);
                            }
                            if (base_1.length > 0) {
                                ((_this.map)).set(update.param, base_1);
                            }
                            else {
                                ((_this.map)).delete(update.param);
                            }
                        }
                        else {
                            ((_this.map)).delete(update.param);
                            break;
                        }
                }
            });
            this.cloneFrom = null;
        }
    };
    return HttpParams;
}());
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Immutable set of Http headers, with lazy parsing.
 * \@experimental
 */
var HttpHeaders = (function () {
    /**
     * @param {?=} headers
     */
    function HttpHeaders(headers) {
        var _this = this;
        /**
         * Internal map of lowercased header names to the normalized
         * form of the name (the form seen first).
         */
        this.normalizedNames = new Map();
        /**
         * Queued updates to be materialized the next initialization.
         */
        this.lazyUpdate = null;
        if (!headers) {
            this.headers = new Map();
        }
        else if (typeof headers === 'string') {
            this.lazyInit = function () {
                _this.headers = new Map();
                headers.split('\n').forEach(function (line) {
                    var index = line.indexOf(':');
                    if (index > 0) {
                        var name = line.slice(0, index);
                        var key = name.toLowerCase();
                        var value = line.slice(index + 1).trim();
                        _this.maybeSetNormalizedName(name, key);
                        if (_this.headers.has(key)) {
                            _this.headers.get(key).push(value);
                        }
                        else {
                            _this.headers.set(key, [value]);
                        }
                    }
                });
            };
        }
        else {
            this.lazyInit = function () {
                _this.headers = new Map();
                Object.keys(headers).forEach(function (name) {
                    var values = headers[name];
                    var key = name.toLowerCase();
                    if (typeof values === 'string') {
                        values = [values];
                    }
                    if (values.length > 0) {
                        _this.headers.set(key, values);
                        _this.maybeSetNormalizedName(name, key);
                    }
                });
            };
        }
    }
    /**
     * Checks for existence of header by given name.
     * @param {?} name
     * @return {?}
     */
    HttpHeaders.prototype.has = function (name) {
        this.init();
        return this.headers.has(name.toLowerCase());
    };
    /**
     * Returns first header that matches given name.
     * @param {?} name
     * @return {?}
     */
    HttpHeaders.prototype.get = function (name) {
        this.init();
        var /** @type {?} */ values = this.headers.get(name.toLowerCase());
        return values && values.length > 0 ? values[0] : null;
    };
    /**
     * Returns the names of the headers
     * @return {?}
     */
    HttpHeaders.prototype.keys = function () {
        this.init();
        return Array.from(this.normalizedNames.values());
    };
    /**
     * Returns list of header values for a given name.
     * @param {?} name
     * @return {?}
     */
    HttpHeaders.prototype.getAll = function (name) {
        this.init();
        return this.headers.get(name.toLowerCase()) || null;
    };
    /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    HttpHeaders.prototype.append = function (name, value) {
        return this.clone({ name: name, value: value, op: 'a' });
    };
    /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    HttpHeaders.prototype.set = function (name, value) {
        return this.clone({ name: name, value: value, op: 's' });
    };
    /**
     * @param {?} name
     * @param {?=} value
     * @return {?}
     */
    HttpHeaders.prototype.delete = function (name, value) {
        return this.clone({ name: name, value: value, op: 'd' });
    };
    /**
     * @param {?} name
     * @param {?} lcName
     * @return {?}
     */
    HttpHeaders.prototype.maybeSetNormalizedName = function (name, lcName) {
        if (!this.normalizedNames.has(lcName)) {
            this.normalizedNames.set(lcName, name);
        }
    };
    /**
     * @return {?}
     */
    HttpHeaders.prototype.init = function () {
        var _this = this;
        if (!!this.lazyInit) {
            if (this.lazyInit instanceof HttpHeaders) {
                this.copyFrom(this.lazyInit);
            }
            else {
                this.lazyInit();
            }
            this.lazyInit = null;
            if (!!this.lazyUpdate) {
                this.lazyUpdate.forEach(function (update) { return _this.applyUpdate(update); });
                this.lazyUpdate = null;
            }
        }
    };
    /**
     * @param {?} other
     * @return {?}
     */
    HttpHeaders.prototype.copyFrom = function (other) {
        var _this = this;
        other.init();
        Array.from(other.headers.keys()).forEach(function (key) {
            _this.headers.set(key, /** @type {?} */ ((other.headers.get(key))));
            _this.normalizedNames.set(key, /** @type {?} */ ((other.normalizedNames.get(key))));
        });
    };
    /**
     * @param {?} update
     * @return {?}
     */
    HttpHeaders.prototype.clone = function (update) {
        var /** @type {?} */ clone = new HttpHeaders();
        clone.lazyInit =
            (!!this.lazyInit && this.lazyInit instanceof HttpHeaders) ? this.lazyInit : this;
        clone.lazyUpdate = (this.lazyUpdate || []).concat([update]);
        return clone;
    };
    /**
     * @param {?} update
     * @return {?}
     */
    HttpHeaders.prototype.applyUpdate = function (update) {
        var /** @type {?} */ key = update.name.toLowerCase();
        switch (update.op) {
            case 'a':
            case 's':
                var /** @type {?} */ value = ((update.value));
                if (typeof value === 'string') {
                    value = [value];
                }
                if (value.length === 0) {
                    return;
                }
                this.maybeSetNormalizedName(update.name, key);
                var /** @type {?} */ base = (update.op === 'a' ? this.headers.get(key) : undefined) || [];
                base.push.apply(base, value);
                this.headers.set(key, base);
                break;
            case 'd':
                var /** @type {?} */ toDelete_1 = (update.value);
                if (!toDelete_1) {
                    this.headers.delete(key);
                    this.normalizedNames.delete(key);
                }
                else {
                    var /** @type {?} */ existing = this.headers.get(key);
                    if (!existing) {
                        return;
                    }
                    existing = existing.filter(function (value) { return toDelete_1.indexOf(value) === -1; });
                    if (existing.length === 0) {
                        this.headers.delete(key);
                        this.normalizedNames.delete(key);
                    }
                    else {
                        this.headers.set(key, existing);
                    }
                }
                break;
        }
    };
    /**
     * \@internal
     * @param {?} fn
     * @return {?}
     */
    HttpHeaders.prototype.forEach = function (fn) {
        var _this = this;
        this.init();
        Array.from(this.normalizedNames.keys())
            .forEach(function (key) { return fn(/** @type {?} */ ((_this.normalizedNames.get(key))), /** @type {?} */ ((_this.headers.get(key)))); });
    };
    return HttpHeaders;
}());
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Determine whether the given HTTP method may include a body.
 * @param {?} method
 * @return {?}
 */
function mightHaveBody(method) {
    switch (method) {
        case 'DELETE':
        case 'GET':
        case 'HEAD':
        case 'OPTIONS':
        case 'JSONP':
            return false;
        default:
            return true;
    }
}
/**
 * Safely assert whether the given value is an ArrayBuffer.
 *
 * In some execution environments ArrayBuffer is not defined.
 * @param {?} value
 * @return {?}
 */
function isArrayBuffer(value) {
    return typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer;
}
/**
 * Safely assert whether the given value is a Blob.
 *
 * In some execution environments Blob is not defined.
 * @param {?} value
 * @return {?}
 */
function isBlob(value) {
    return typeof Blob !== 'undefined' && value instanceof Blob;
}
/**
 * Safely assert whether the given value is a FormData instance.
 *
 * In some execution environments FormData is not defined.
 * @param {?} value
 * @return {?}
 */
function isFormData(value) {
    return typeof FormData !== 'undefined' && value instanceof FormData;
}
/**
 * An outgoing HTTP request with an optional typed body.
 *
 * `HttpRequest` represents an outgoing request, including URL, method,
 * headers, body, and other request configuration options. Instances should be
 * assumed to be immutable. To modify a `HttpRequest`, the `clone`
 * method should be used.
 *
 * \@experimental
 */
var HttpRequest = (function () {
    /**
     * @param {?} method
     * @param {?} url
     * @param {?=} third
     * @param {?=} fourth
     */
    function HttpRequest(method, url, third, fourth) {
        this.url = url;
        /**
         * The request body, or `null` if one isn't set.
         *
         * Bodies are not enforced to be immutable, as they can include a reference to any
         * user-defined data type. However, interceptors should take care to preserve
         * idempotence by treating them as such.
         */
        this.body = null;
        /**
         * Whether this request should be made in a way that exposes progress events.
         *
         * Progress events are expensive (change detection runs on each event) and so
         * they should only be requested if the consumer intends to monitor them.
         */
        this.reportProgress = false;
        /**
         * Whether this request should be sent with outgoing credentials (cookies).
         */
        this.withCredentials = false;
        /**
         * The expected response type of the server.
         *
         * This is used to parse the response appropriately before returning it to
         * the requestee.
         */
        this.responseType = 'json';
        this.method = method.toUpperCase();
        // Next, need to figure out which argument holds the HttpRequestInit
        // options, if any.
        var options;
        // Check whether a body argument is expected. The only valid way to omit
        // the body argument is to use a known no-body method like GET.
        if (mightHaveBody(this.method) || !!fourth) {
            // Body is the third argument, options are the fourth.
            this.body = third || null;
            options = fourth;
        }
        else {
            // No body required, options are the third argument. The body stays null.
            options = third;
        }
        // If options have been passed, interpret them.
        if (options) {
            // Normalize reportProgress and withCredentials.
            this.reportProgress = !!options.reportProgress;
            this.withCredentials = !!options.withCredentials;
            // Override default response type of 'json' if one is provided.
            if (!!options.responseType) {
                this.responseType = options.responseType;
            }
            // Override headers if they're provided.
            if (!!options.headers) {
                this.headers = options.headers;
            }
            if (!!options.params) {
                this.params = options.params;
            }
        }
        // If no headers have been passed in, construct a new HttpHeaders instance.
        if (!this.headers) {
            this.headers = new HttpHeaders();
        }
        // If no parameters have been passed in, construct a new HttpUrlEncodedParams instance.
        if (!this.params) {
            this.params = new HttpParams();
            this.urlWithParams = url;
        }
        else {
            // Encode the parameters to a string in preparation for inclusion in the URL.
            var params = this.params.toString();
            if (params.length === 0) {
                // No parameters, the visible URL is just the URL given at creation time.
                this.urlWithParams = url;
            }
            else {
                // Does the URL already have query parameters? Look for '?'.
                var qIdx = url.indexOf('?');
                // There are 3 cases to handle:
                // 1) No existing parameters -> append '?' followed by params.
                // 2) '?' exists and is followed by existing query string ->
                //    append '&' followed by params.
                // 3) '?' exists at the end of the url -> append params directly.
                // This basically amounts to determining the character, if any, with
                // which to join the URL and parameters.
                var sep = qIdx === -1 ? '?' : (qIdx < url.length - 1 ? '&' : '');
                this.urlWithParams = url + sep + params;
            }
        }
    }
    /**
     * Transform the free-form body into a serialized format suitable for
     * transmission to the server.
     * @return {?}
     */
    HttpRequest.prototype.serializeBody = function () {
        // If no body is present, no need to serialize it.
        if (this.body === null) {
            return null;
        }
        // Check whether the body is already in a serialized form. If so,
        // it can just be returned directly.
        if (isArrayBuffer(this.body) || isBlob(this.body) || isFormData(this.body) ||
            typeof this.body === 'string') {
            return this.body;
        }
        // Check whether the body is an instance of HttpUrlEncodedParams.
        if (this.body instanceof HttpParams) {
            return this.body.toString();
        }
        // Check whether the body is an object or array, and serialize with JSON if so.
        if (typeof this.body === 'object' || typeof this.body === 'boolean' ||
            Array.isArray(this.body)) {
            return JSON.stringify(this.body);
        }
        // Fall back on toString() for everything else.
        return ((this.body)).toString();
    };
    /**
     * Examine the body and attempt to infer an appropriate MIME type
     * for it.
     *
     * If no such type can be inferred, this method will return `null`.
     * @return {?}
     */
    HttpRequest.prototype.detectContentTypeHeader = function () {
        // An empty body has no content type.
        if (this.body === null) {
            return null;
        }
        // FormData bodies rely on the browser's content type assignment.
        if (isFormData(this.body)) {
            return null;
        }
        // Blobs usually have their own content type. If it doesn't, then
        // no type can be inferred.
        if (isBlob(this.body)) {
            return this.body.type || null;
        }
        // Array buffers have unknown contents and thus no type can be inferred.
        if (isArrayBuffer(this.body)) {
            return null;
        }
        // Technically, strings could be a form of JSON data, but it's safe enough
        // to assume they're plain strings.
        if (typeof this.body === 'string') {
            return 'text/plain';
        }
        // `HttpUrlEncodedParams` has its own content-type.
        if (this.body instanceof HttpParams) {
            return 'application/x-www-form-urlencoded;charset=UTF-8';
        }
        // Arrays, objects, and numbers will be encoded as JSON.
        if (typeof this.body === 'object' || typeof this.body === 'number' ||
            Array.isArray(this.body)) {
            return 'application/json';
        }
        // No type could be inferred.
        return null;
    };
    /**
     * @param {?=} update
     * @return {?}
     */
    HttpRequest.prototype.clone = function (update) {
        if (update === void 0) { update = {}; }
        // For method, url, and responseType, take the current value unless
        // it is overridden in the update hash.
        var /** @type {?} */ method = update.method || this.method;
        var /** @type {?} */ url = update.url || this.url;
        var /** @type {?} */ responseType = update.responseType || this.responseType;
        // The body is somewhat special - a `null` value in update.body means
        // whatever current body is present is being overridden with an empty
        // body, whereas an `undefined` value in update.body implies no
        // override.
        var /** @type {?} */ body = (update.body !== undefined) ? update.body : this.body;
        // Carefully handle the boolean options to differentiate between
        // `false` and `undefined` in the update args.
        var /** @type {?} */ withCredentials = (update.withCredentials !== undefined) ? update.withCredentials : this.withCredentials;
        var /** @type {?} */ reportProgress = (update.reportProgress !== undefined) ? update.reportProgress : this.reportProgress;
        // Headers and params may be appended to if `setHeaders` or
        // `setParams` are used.
        var /** @type {?} */ headers = update.headers || this.headers;
        var /** @type {?} */ params = update.params || this.params;
        // Check whether the caller has asked to add headers.
        if (update.setHeaders !== undefined) {
            // Set every requested header.
            headers =
                Object.keys(update.setHeaders)
                    .reduce(function (headers, name) { return headers.set(name, /** @type {?} */ ((update.setHeaders))[name]); }, headers);
        }
        // Check whether the caller has asked to set params.
        if (update.setParams) {
            // Set every requested param.
            params = Object.keys(update.setParams)
                .reduce(function (params, param) { return params.set(param, /** @type {?} */ ((update.setParams))[param]); }, params);
        }
        // Finally, construct the new HttpRequest using the pieces from above.
        return new HttpRequest(method, url, body, {
            params: params, headers: headers, reportProgress: reportProgress, responseType: responseType, withCredentials: withCredentials,
        });
    };
    return HttpRequest;
}());
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
var HttpEventType = {};
HttpEventType.Sent = 0;
HttpEventType.UploadProgress = 1;
HttpEventType.ResponseHeader = 2;
HttpEventType.DownloadProgress = 3;
HttpEventType.Response = 4;
HttpEventType.User = 5;
HttpEventType[HttpEventType.Sent] = "Sent";
HttpEventType[HttpEventType.UploadProgress] = "UploadProgress";
HttpEventType[HttpEventType.ResponseHeader] = "ResponseHeader";
HttpEventType[HttpEventType.DownloadProgress] = "DownloadProgress";
HttpEventType[HttpEventType.Response] = "Response";
HttpEventType[HttpEventType.User] = "User";
/**
 * Base class for both `HttpResponse` and `HttpHeaderResponse`.
 *
 * \@experimental
 * @abstract
 */
var HttpResponseBase = (function () {
    /**
     * Super-constructor for all responses.
     *
     * The single parameter accepted is an initialization hash. Any properties
     * of the response passed there will override the default values.
     * @param {?} init
     * @param {?=} defaultStatus
     * @param {?=} defaultStatusText
     */
    function HttpResponseBase(init, defaultStatus, defaultStatusText) {
        if (defaultStatus === void 0) { defaultStatus = 200; }
        if (defaultStatusText === void 0) { defaultStatusText = 'OK'; }
        // If the hash has values passed, use them to initialize the response.
        // Otherwise use the default values.
        this.headers = init.headers || new HttpHeaders();
        this.status = init.status !== undefined ? init.status : defaultStatus;
        this.statusText = init.statusText || defaultStatusText;
        this.url = init.url || null;
        // Cache the ok value to avoid defining a getter.
        this.ok = this.status >= 200 && this.status < 300;
    }
    return HttpResponseBase;
}());
/**
 * A partial HTTP response which only includes the status and header data,
 * but no response body.
 *
 * `HttpHeaderResponse` is a `HttpEvent` available on the response
 * event stream, only when progress events are requested.
 *
 * \@experimental
 */
var HttpHeaderResponse = (function (_super) {
    __WEBPACK_IMPORTED_MODULE_0_tslib__["a" /* __extends */](HttpHeaderResponse, _super);
    /**
     * Create a new `HttpHeaderResponse` with the given parameters.
     * @param {?=} init
     */
    function HttpHeaderResponse(init) {
        if (init === void 0) { init = {}; }
        var _this = _super.call(this, init) || this;
        _this.type = HttpEventType.ResponseHeader;
        return _this;
    }
    /**
     * Copy this `HttpHeaderResponse`, overriding its contents with the
     * given parameter hash.
     * @param {?=} update
     * @return {?}
     */
    HttpHeaderResponse.prototype.clone = function (update) {
        if (update === void 0) { update = {}; }
        // Perform a straightforward initialization of the new HttpHeaderResponse,
        // overriding the current parameters with new ones if given.
        return new HttpHeaderResponse({
            headers: update.headers || this.headers,
            status: update.status !== undefined ? update.status : this.status,
            statusText: update.statusText || this.statusText,
            url: update.url || this.url || undefined,
        });
    };
    return HttpHeaderResponse;
}(HttpResponseBase));
/**
 * A full HTTP response, including a typed response body (which may be `null`
 * if one was not returned).
 *
 * `HttpResponse` is a `HttpEvent` available on the response event
 * stream.
 *
 * \@experimental
 */
var HttpResponse = (function (_super) {
    __WEBPACK_IMPORTED_MODULE_0_tslib__["a" /* __extends */](HttpResponse, _super);
    /**
     * Construct a new `HttpResponse`.
     * @param {?=} init
     */
    function HttpResponse(init) {
        if (init === void 0) { init = {}; }
        var _this = _super.call(this, init) || this;
        _this.type = HttpEventType.Response;
        _this.body = init.body || null;
        return _this;
    }
    /**
     * @param {?=} update
     * @return {?}
     */
    HttpResponse.prototype.clone = function (update) {
        if (update === void 0) { update = {}; }
        return new HttpResponse({
            body: (update.body !== undefined) ? update.body : this.body,
            headers: update.headers || this.headers,
            status: (update.status !== undefined) ? update.status : this.status,
            statusText: update.statusText || this.statusText,
            url: update.url || this.url || undefined,
        });
    };
    return HttpResponse;
}(HttpResponseBase));
/**
 * A response that represents an error or failure, either from a
 * non-successful HTTP status, an error while executing the request,
 * or some other failure which occurred during the parsing of the response.
 *
 * Any error returned on the `Observable` response stream will be
 * wrapped in an `HttpErrorResponse` to provide additional context about
 * the state of the HTTP layer when the error occurred. The error property
 * will contain either a wrapped Error object or the error response returned
 * from the server.
 *
 * \@experimental
 */
var HttpErrorResponse = (function (_super) {
    __WEBPACK_IMPORTED_MODULE_0_tslib__["a" /* __extends */](HttpErrorResponse, _super);
    /**
     * @param {?} init
     */
    function HttpErrorResponse(init) {
        var _this = 
        // Initialize with a default status of 0 / Unknown Error.
        _super.call(this, init, 0, 'Unknown Error') || this;
        _this.name = 'HttpErrorResponse';
        /**
         * Errors are never okay, even when the status code is in the 2xx success range.
         */
        _this.ok = false;
        // If the response was successful, then this was a parse error. Otherwise, it was
        // a protocol-level failure of some sort. Either the request failed in transit
        // or the server returned an unsuccessful status code.
        if (_this.status >= 200 && _this.status < 300) {
            _this.message = "Http failure during parsing for " + (init.url || '(unknown url)');
        }
        else {
            _this.message =
                "Http failure response for " + (init.url || '(unknown url)') + ": " + init.status + " " + init.statusText;
        }
        _this.error = init.error || null;
        return _this;
    }
    return HttpErrorResponse;
}(HttpResponseBase));
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Construct an instance of `HttpRequestOptions<T>` from a source `HttpMethodOptions` and
 * the given `body`. Basically, this clones the object and adds the body.
 * @template T
 * @param {?} options
 * @param {?} body
 * @return {?}
 */
function addBody(options, body) {
    return {
        body: body,
        headers: options.headers,
        observe: options.observe,
        params: options.params,
        reportProgress: options.reportProgress,
        responseType: options.responseType,
        withCredentials: options.withCredentials,
    };
}
/**
 * Perform HTTP requests.
 *
 * `HttpClient` is available as an injectable class, with methods to perform HTTP requests.
 * Each request method has multiple signatures, and the return type varies according to which
 * signature is called (mainly the values of `observe` and `responseType`).
 *
 * \@experimental
 */
var HttpClient = (function () {
    /**
     * @param {?} handler
     */
    function HttpClient(handler) {
        this.handler = handler;
    }
    /**
     * Constructs an `Observable` for a particular HTTP request that, when subscribed,
     * fires the request through the chain of registered interceptors and on to the
     * server.
     *
     * This method can be called in one of two ways. Either an `HttpRequest`
     * instance can be passed directly as the only parameter, or a method can be
     * passed as the first parameter, a string URL as the second, and an
     * options hash as the third.
     *
     * If a `HttpRequest` object is passed directly, an `Observable` of the
     * raw `HttpEvent` stream will be returned.
     *
     * If a request is instead built by providing a URL, the options object
     * determines the return type of `request()`. In addition to configuring
     * request parameters such as the outgoing headers and/or the body, the options
     * hash specifies two key pieces of information about the request: the
     * `responseType` and what to `observe`.
     *
     * The `responseType` value determines how a successful response body will be
     * parsed. If `responseType` is the default `json`, a type interface for the
     * resulting object may be passed as a type parameter to `request()`.
     *
     * The `observe` value determines the return type of `request()`, based on what
     * the consumer is interested in observing. A value of `events` will return an
     * `Observable<HttpEvent>` representing the raw `HttpEvent` stream,
     * including progress events by default. A value of `response` will return an
     * `Observable<HttpResponse<T>>` where the `T` parameter of `HttpResponse`
     * depends on the `responseType` and any optionally provided type parameter.
     * A value of `body` will return an `Observable<T>` with the same `T` body type.
     * @param {?} first
     * @param {?=} url
     * @param {?=} options
     * @return {?}
     */
    HttpClient.prototype.request = function (first, url, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var /** @type {?} */ req;
        // Firstly, check whether the primary argument is an instance of `HttpRequest`.
        if (first instanceof HttpRequest) {
            // It is. The other arguments must be undefined (per the signatures) and can be
            // ignored.
            req = (first);
        }
        else {
            // It's a string, so it represents a URL. Construct a request based on it,
            // and incorporate the remaining arguments (assuming GET unless a method is
            // provided.
            req = new HttpRequest(first, /** @type {?} */ ((url)), options.body || null, {
                headers: options.headers,
                params: options.params,
                reportProgress: options.reportProgress,
                // By default, JSON is assumed to be returned for all calls.
                responseType: options.responseType || 'json',
                withCredentials: options.withCredentials,
            });
        }
        // Start with an Observable.of() the initial request, and run the handler (which
        // includes all interceptors) inside a concatMap(). This way, the handler runs
        // inside an Observable chain, which causes interceptors to be re-run on every
        // subscription (this also makes retries re-run the handler, including interceptors).
        var /** @type {?} */ events$ = __WEBPACK_IMPORTED_MODULE_3_rxjs_operator_concatMap__["concatMap"].call(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__["of"])(req), function (req) { return _this.handler.handle(req); });
        // If coming via the API signature which accepts a previously constructed HttpRequest,
        // the only option is to get the event stream. Otherwise, return the event stream if
        // that is what was requested.
        if (first instanceof HttpRequest || options.observe === 'events') {
            return events$;
        }
        // The requested stream contains either the full response or the body. In either
        // case, the first step is to filter the event stream to extract a stream of
        // responses(s).
        var /** @type {?} */ res$ = __WEBPACK_IMPORTED_MODULE_4_rxjs_operator_filter__["filter"].call(events$, function (event) { return event instanceof HttpResponse; });
        // Decide which stream to return.
        switch (options.observe || 'body') {
            case 'body':
                // The requested stream is the body. Map the response stream to the response
                // body. This could be done more simply, but a misbehaving interceptor might
                // transform the response body into a different format and ignore the requested
                // responseType. Guard against this by validating that the response is of the
                // requested type.
                switch (req.responseType) {
                    case 'arraybuffer':
                        return __WEBPACK_IMPORTED_MODULE_5_rxjs_operator_map__["map"].call(res$, function (res) {
                            // Validate that the body is an ArrayBuffer.
                            if (res.body !== null && !(res.body instanceof ArrayBuffer)) {
                                throw new Error('Response is not an ArrayBuffer.');
                            }
                            return res.body;
                        });
                    case 'blob':
                        return __WEBPACK_IMPORTED_MODULE_5_rxjs_operator_map__["map"].call(res$, function (res) {
                            // Validate that the body is a Blob.
                            if (res.body !== null && !(res.body instanceof Blob)) {
                                throw new Error('Response is not a Blob.');
                            }
                            return res.body;
                        });
                    case 'text':
                        return __WEBPACK_IMPORTED_MODULE_5_rxjs_operator_map__["map"].call(res$, function (res) {
                            // Validate that the body is a string.
                            if (res.body !== null && typeof res.body !== 'string') {
                                throw new Error('Response is not a string.');
                            }
                            return res.body;
                        });
                    case 'json':
                    default:
                        // No validation needed for JSON responses, as they can be of any type.
                        return __WEBPACK_IMPORTED_MODULE_5_rxjs_operator_map__["map"].call(res$, function (res) { return res.body; });
                }
            case 'response':
                // The response stream was requested directly, so return it.
                return res$;
            default:
                // Guard against new future observe types being added.
                throw new Error("Unreachable: unhandled observe type " + options.observe + "}");
        }
    };
    /**
     * Constructs an `Observable` which, when subscribed, will cause the configured
     * DELETE request to be executed on the server. See the individual overloads for
     * details of `delete()`'s return type based on the provided options.
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    HttpClient.prototype.delete = function (url, options) {
        if (options === void 0) { options = {}; }
        return this.request('DELETE', url, /** @type {?} */ (options));
    };
    /**
     * Constructs an `Observable` which, when subscribed, will cause the configured
     * GET request to be executed on the server. See the individual overloads for
     * details of `get()`'s return type based on the provided options.
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    HttpClient.prototype.get = function (url, options) {
        if (options === void 0) { options = {}; }
        return this.request('GET', url, /** @type {?} */ (options));
    };
    /**
     * Constructs an `Observable` which, when subscribed, will cause the configured
     * HEAD request to be executed on the server. See the individual overloads for
     * details of `head()`'s return type based on the provided options.
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    HttpClient.prototype.head = function (url, options) {
        if (options === void 0) { options = {}; }
        return this.request('HEAD', url, /** @type {?} */ (options));
    };
    /**
     * Constructs an `Observable` which, when subscribed, will cause a request
     * with the special method `JSONP` to be dispatched via the interceptor pipeline.
     *
     * A suitable interceptor must be installed (e.g. via the `HttpClientJsonpModule`).
     * If no such interceptor is reached, then the `JSONP` request will likely be
     * rejected by the configured backend.
     * @template T
     * @param {?} url
     * @param {?} callbackParam
     * @return {?}
     */
    HttpClient.prototype.jsonp = function (url, callbackParam) {
        return this.request('JSONP', url, {
            params: new HttpParams().append(callbackParam, 'JSONP_CALLBACK'),
            observe: 'body',
            responseType: 'json',
        });
    };
    /**
     * Constructs an `Observable` which, when subscribed, will cause the configured
     * OPTIONS request to be executed on the server. See the individual overloads for
     * details of `options()`'s return type based on the provided options.
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    HttpClient.prototype.options = function (url, options) {
        if (options === void 0) { options = {}; }
        return this.request('OPTIONS', url, /** @type {?} */ (options));
    };
    /**
     * Constructs an `Observable` which, when subscribed, will cause the configured
     * PATCH request to be executed on the server. See the individual overloads for
     * details of `patch()`'s return type based on the provided options.
     * @param {?} url
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    HttpClient.prototype.patch = function (url, body, options) {
        if (options === void 0) { options = {}; }
        return this.request('PATCH', url, addBody(options, body));
    };
    /**
     * Constructs an `Observable` which, when subscribed, will cause the configured
     * POST request to be executed on the server. See the individual overloads for
     * details of `post()`'s return type based on the provided options.
     * @param {?} url
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    HttpClient.prototype.post = function (url, body, options) {
        if (options === void 0) { options = {}; }
        return this.request('POST', url, addBody(options, body));
    };
    /**
     * Constructs an `Observable` which, when subscribed, will cause the configured
     * POST request to be executed on the server. See the individual overloads for
     * details of `post()`'s return type based on the provided options.
     * @param {?} url
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    HttpClient.prototype.put = function (url, body, options) {
        if (options === void 0) { options = {}; }
        return this.request('PUT', url, addBody(options, body));
    };
    return HttpClient;
}());
HttpClient.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"] },
];
/**
 * @nocollapse
 */
HttpClient.ctorParameters = function () { return [
    { type: HttpHandler, },
]; };
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * `HttpHandler` which applies an `HttpInterceptor` to an `HttpRequest`.
 *
 * \@experimental
 */
var HttpInterceptorHandler = (function () {
    /**
     * @param {?} next
     * @param {?} interceptor
     */
    function HttpInterceptorHandler(next, interceptor) {
        this.next = next;
        this.interceptor = interceptor;
    }
    /**
     * @param {?} req
     * @return {?}
     */
    HttpInterceptorHandler.prototype.handle = function (req) {
        return this.interceptor.intercept(req, this.next);
    };
    return HttpInterceptorHandler;
}());
/**
 * A multi-provider token which represents the array of `HttpInterceptor`s that
 * are registered.
 *
 * \@experimental
 */
var HTTP_INTERCEPTORS = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["InjectionToken"]('HTTP_INTERCEPTORS');
var NoopInterceptor = (function () {
    function NoopInterceptor() {
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    NoopInterceptor.prototype.intercept = function (req, next) {
        return next.handle(req);
    };
    return NoopInterceptor;
}());
NoopInterceptor.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"] },
];
/**
 * @nocollapse
 */
NoopInterceptor.ctorParameters = function () { return []; };
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
// Every request made through JSONP needs a callback name that's unique across the
// whole page. Each request is assigned an id and the callback name is constructed
// from that. The next id to be assigned is tracked in a global variable here that
// is shared among all applications on the page.
var nextRequestId = 0;
// Error text given when a JSONP script is injected, but doesn't invoke the callback
// passed in its URL.
var JSONP_ERR_NO_CALLBACK = 'JSONP injected script did not invoke callback.';
// Error text given when a request is passed to the JsonpClientBackend that doesn't
// have a request method JSONP.
var JSONP_ERR_WRONG_METHOD = 'JSONP requests must use JSONP request method.';
var JSONP_ERR_WRONG_RESPONSE_TYPE = 'JSONP requests must use Json response type.';
/**
 * DI token/abstract type representing a map of JSONP callbacks.
 *
 * In the browser, this should always be the `window` object.
 *
 * \@experimental
 * @abstract
 */
var JsonpCallbackContext = (function () {
    function JsonpCallbackContext() {
    }
    return JsonpCallbackContext;
}());
/**
 * `HttpBackend` that only processes `HttpRequest` with the JSONP method,
 * by performing JSONP style requests.
 *
 * \@experimental
 */
var JsonpClientBackend = (function () {
    /**
     * @param {?} callbackMap
     * @param {?} document
     */
    function JsonpClientBackend(callbackMap, document) {
        this.callbackMap = callbackMap;
        this.document = document;
    }
    /**
     * Get the name of the next callback method, by incrementing the global `nextRequestId`.
     * @return {?}
     */
    JsonpClientBackend.prototype.nextCallback = function () { return "ng_jsonp_callback_" + nextRequestId++; };
    /**
     * Process a JSONP request and return an event stream of the results.
     * @param {?} req
     * @return {?}
     */
    JsonpClientBackend.prototype.handle = function (req) {
        var _this = this;
        // Firstly, check both the method and response type. If either doesn't match
        // then the request was improperly routed here and cannot be handled.
        if (req.method !== 'JSONP') {
            throw new Error(JSONP_ERR_WRONG_METHOD);
        }
        else if (req.responseType !== 'json') {
            throw new Error(JSONP_ERR_WRONG_RESPONSE_TYPE);
        }
        // Everything else happens inside the Observable boundary.
        return new __WEBPACK_IMPORTED_MODULE_7_rxjs_Observable__["Observable"](function (observer) {
            // The first step to make a request is to generate the callback name, and replace the
            // callback placeholder in the URL with the name. Care has to be taken here to ensure
            // a trailing &, if matched, gets inserted back into the URL in the correct place.
            var /** @type {?} */ callback = _this.nextCallback();
            var /** @type {?} */ url = req.urlWithParams.replace(/=JSONP_CALLBACK(&|$)/, "=" + callback + "$1");
            // Construct the <script> tag and point it at the URL.
            var /** @type {?} */ node = _this.document.createElement('script');
            node.src = url;
            // A JSONP request requires waiting for multiple callbacks. These variables
            // are closed over and track state across those callbacks.
            // The response object, if one has been received, or null otherwise.
            var /** @type {?} */ body = null;
            // Whether the response callback has been called.
            var /** @type {?} */ finished = false;
            // Whether the request has been cancelled (and thus any other callbacks)
            // should be ignored.
            var /** @type {?} */ cancelled = false;
            // Set the response callback in this.callbackMap (which will be the window
            // object in the browser. The script being loaded via the <script> tag will
            // eventually call this callback.
            _this.callbackMap[callback] = function (data) {
                // Data has been received from the JSONP script. Firstly, delete this callback.
                delete _this.callbackMap[callback];
                // Next, make sure the request wasn't cancelled in the meantime.
                if (cancelled) {
                    return;
                }
                // Set state to indicate data was received.
                body = data;
                finished = true;
            };
            // cleanup() is a utility closure that removes the <script> from the page and
            // the response callback from the window. This logic is used in both the
            // success, error, and cancellation paths, so it's extracted out for convenience.
            var /** @type {?} */ cleanup = function () {
                // Remove the <script> tag if it's still on the page.
                if (node.parentNode) {
                    node.parentNode.removeChild(node);
                }
                // Remove the response callback from the callbackMap (window object in the
                // browser).
                delete _this.callbackMap[callback];
            };
            // onLoad() is the success callback which runs after the response callback
            // if the JSONP script loads successfully. The event itself is unimportant.
            // If something went wrong, onLoad() may run without the response callback
            // having been invoked.
            var /** @type {?} */ onLoad = function (event) {
                // Do nothing if the request has been cancelled.
                if (cancelled) {
                    return;
                }
                // Cleanup the page.
                cleanup();
                // Check whether the response callback has run.
                if (!finished) {
                    // It hasn't, something went wrong with the request. Return an error via
                    // the Observable error path. All JSONP errors have status 0.
                    observer.error(new HttpErrorResponse({
                        url: url,
                        status: 0,
                        statusText: 'JSONP Error',
                        error: new Error(JSONP_ERR_NO_CALLBACK),
                    }));
                    return;
                }
                // Success. body either contains the response body or null if none was
                // returned.
                observer.next(new HttpResponse({
                    body: body,
                    status: 200,
                    statusText: 'OK', url: url,
                }));
                // Complete the stream, the resposne is over.
                observer.complete();
            };
            // onError() is the error callback, which runs if the script returned generates
            // a Javascript error. It emits the error via the Observable error channel as
            // a HttpErrorResponse.
            var /** @type {?} */ onError = function (error) {
                // If the request was already cancelled, no need to emit anything.
                if (cancelled) {
                    return;
                }
                cleanup();
                // Wrap the error in a HttpErrorResponse.
                observer.error(new HttpErrorResponse({
                    error: error,
                    status: 0,
                    statusText: 'JSONP Error', url: url,
                }));
            };
            // Subscribe to both the success (load) and error events on the <script> tag,
            // and add it to the page.
            node.addEventListener('load', onLoad);
            node.addEventListener('error', onError);
            _this.document.body.appendChild(node);
            // The request has now been successfully sent.
            observer.next({ type: HttpEventType.Sent });
            // Cancellation handler.
            return function () {
                // Track the cancellation so event listeners won't do anything even if already scheduled.
                cancelled = true;
                // Remove the event listeners so they won't run if the events later fire.
                node.removeEventListener('load', onLoad);
                node.removeEventListener('error', onError);
                // And finally, clean up the page.
                cleanup();
            };
        });
    };
    return JsonpClientBackend;
}());
JsonpClientBackend.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"] },
];
/**
 * @nocollapse
 */
JsonpClientBackend.ctorParameters = function () { return [
    { type: JsonpCallbackContext, },
    { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"], args: [__WEBPACK_IMPORTED_MODULE_6__angular_common__["DOCUMENT"],] },] },
]; };
/**
 * An `HttpInterceptor` which identifies requests with the method JSONP and
 * shifts them to the `JsonpClientBackend`.
 *
 * \@experimental
 */
var JsonpInterceptor = (function () {
    /**
     * @param {?} jsonp
     */
    function JsonpInterceptor(jsonp) {
        this.jsonp = jsonp;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    JsonpInterceptor.prototype.intercept = function (req, next) {
        if (req.method === 'JSONP') {
            return this.jsonp.handle(/** @type {?} */ (req));
        }
        // Fall through for normal HTTP requests.
        return next.handle(req);
    };
    return JsonpInterceptor;
}());
JsonpInterceptor.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"] },
];
/**
 * @nocollapse
 */
JsonpInterceptor.ctorParameters = function () { return [
    { type: JsonpClientBackend, },
]; };
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
var XSSI_PREFIX = /^\)\]\}',?\n/;
/**
 * Determine an appropriate URL for the response, by checking either
 * XMLHttpRequest.responseURL or the X-Request-URL header.
 * @param {?} xhr
 * @return {?}
 */
function getResponseUrl(xhr) {
    if ('responseURL' in xhr && xhr.responseURL) {
        return xhr.responseURL;
    }
    if (/^X-Request-URL:/m.test(xhr.getAllResponseHeaders())) {
        return xhr.getResponseHeader('X-Request-URL');
    }
    return null;
}
/**
 * A wrapper around the `XMLHttpRequest` constructor.
 *
 * \@experimental
 * @abstract
 */
var XhrFactory = (function () {
    function XhrFactory() {
    }
    /**
     * @abstract
     * @return {?}
     */
    XhrFactory.prototype.build = function () { };
    return XhrFactory;
}());
/**
 * A factory for \@{link HttpXhrBackend} that uses the `XMLHttpRequest` browser API.
 *
 * \@experimental
 */
var BrowserXhr = (function () {
    function BrowserXhr() {
    }
    /**
     * @return {?}
     */
    BrowserXhr.prototype.build = function () { return ((new XMLHttpRequest())); };
    return BrowserXhr;
}());
BrowserXhr.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"] },
];
/**
 * @nocollapse
 */
BrowserXhr.ctorParameters = function () { return []; };
/**
 * An `HttpBackend` which uses the XMLHttpRequest API to send
 * requests to a backend server.
 *
 * \@experimental
 */
var HttpXhrBackend = (function () {
    /**
     * @param {?} xhrFactory
     */
    function HttpXhrBackend(xhrFactory) {
        this.xhrFactory = xhrFactory;
    }
    /**
     * Process a request and return a stream of response events.
     * @param {?} req
     * @return {?}
     */
    HttpXhrBackend.prototype.handle = function (req) {
        var _this = this;
        // Quick check to give a better error message when a user attempts to use
        // HttpClient.jsonp() without installing the JsonpClientModule
        if (req.method === 'JSONP') {
            throw new Error("Attempted to construct Jsonp request without JsonpClientModule installed.");
        }
        // Everything happens on Observable subscription.
        return new __WEBPACK_IMPORTED_MODULE_7_rxjs_Observable__["Observable"](function (observer) {
            // Start by setting up the XHR object with request method, URL, and withCredentials flag.
            var /** @type {?} */ xhr = _this.xhrFactory.build();
            xhr.open(req.method, req.urlWithParams);
            if (!!req.withCredentials) {
                xhr.withCredentials = true;
            }
            // Add all the requested headers.
            req.headers.forEach(function (name, values) { return xhr.setRequestHeader(name, values.join(',')); });
            // Add an Accept header if one isn't present already.
            if (!req.headers.has('Accept')) {
                xhr.setRequestHeader('Accept', 'application/json, text/plain, */*');
            }
            // Auto-detect the Content-Type header if one isn't present already.
            if (!req.headers.has('Content-Type')) {
                var /** @type {?} */ detectedType = req.detectContentTypeHeader();
                // Sometimes Content-Type detection fails.
                if (detectedType !== null) {
                    xhr.setRequestHeader('Content-Type', detectedType);
                }
            }
            // Set the responseType if one was requested.
            if (req.responseType) {
                var /** @type {?} */ responseType = req.responseType.toLowerCase();
                // JSON responses need to be processed as text. This is because if the server
                // returns an XSSI-prefixed JSON response, the browser will fail to parse it,
                // xhr.response will be null, and xhr.responseText cannot be accessed to
                // retrieve the prefixed JSON data in order to strip the prefix. Thus, all JSON
                // is parsed by first requesting text and then applying JSON.parse.
                xhr.responseType = (((responseType !== 'json') ? responseType : 'text'));
            }
            // Serialize the request body if one is present. If not, this will be set to null.
            var /** @type {?} */ reqBody = req.serializeBody();
            // If progress events are enabled, response headers will be delivered
            // in two events - the HttpHeaderResponse event and the full HttpResponse
            // event. However, since response headers don't change in between these
            // two events, it doesn't make sense to parse them twice. So headerResponse
            // caches the data extracted from the response whenever it's first parsed,
            // to ensure parsing isn't duplicated.
            var /** @type {?} */ headerResponse = null;
            // partialFromXhr extracts the HttpHeaderResponse from the current XMLHttpRequest
            // state, and memoizes it into headerResponse.
            var /** @type {?} */ partialFromXhr = function () {
                if (headerResponse !== null) {
                    return headerResponse;
                }
                // Read status and normalize an IE9 bug (http://bugs.jquery.com/ticket/1450).
                var /** @type {?} */ status = xhr.status === 1223 ? 204 : xhr.status;
                var /** @type {?} */ statusText = xhr.statusText || 'OK';
                // Parse headers from XMLHttpRequest - this step is lazy.
                var /** @type {?} */ headers = new HttpHeaders(xhr.getAllResponseHeaders());
                // Read the response URL from the XMLHttpResponse instance and fall back on the
                // request URL.
                var /** @type {?} */ url = getResponseUrl(xhr) || req.url;
                // Construct the HttpHeaderResponse and memoize it.
                headerResponse = new HttpHeaderResponse({ headers: headers, status: status, statusText: statusText, url: url });
                return headerResponse;
            };
            // Next, a few closures are defined for the various events which XMLHttpRequest can
            // emit. This allows them to be unregistered as event listeners later.
            // First up is the load event, which represents a response being fully available.
            var /** @type {?} */ onLoad = function () {
                // Read response state from the memoized partial data.
                var _a = partialFromXhr(), headers = _a.headers, status = _a.status, statusText = _a.statusText, url = _a.url;
                // The body will be read out if present.
                var /** @type {?} */ body = null;
                if (status !== 204) {
                    // Use XMLHttpRequest.response if set, responseText otherwise.
                    body = (typeof xhr.response === 'undefined') ? xhr.responseText : xhr.response;
                }
                // Normalize another potential bug (this one comes from CORS).
                if (status === 0) {
                    status = !!body ? 200 : 0;
                }
                // ok determines whether the response will be transmitted on the event or
                // error channel. Unsuccessful status codes (not 2xx) will always be errors,
                // but a successful status code can still result in an error if the user
                // asked for JSON data and the body cannot be parsed as such.
                var /** @type {?} */ ok = status >= 200 && status < 300;
                // Check whether the body needs to be parsed as JSON (in many cases the browser
                // will have done that already).
                if (ok && req.responseType === 'json' && typeof body === 'string') {
                    // Attempt the parse. If it fails, a parse error should be delivered to the user.
                    body = body.replace(XSSI_PREFIX, '');
                    try {
                        body = JSON.parse(body);
                    }
                    catch (error) {
                        // Even though the response status was 2xx, this is still an error.
                        ok = false;
                        // The parse error contains the text of the body that failed to parse.
                        body = ({ error: error, text: body });
                    }
                }
                if (ok) {
                    // A successful response is delivered on the event stream.
                    observer.next(new HttpResponse({
                        body: body,
                        headers: headers,
                        status: status,
                        statusText: statusText,
                        url: url || undefined,
                    }));
                    // The full body has been received and delivered, no further events
                    // are possible. This request is complete.
                    observer.complete();
                }
                else {
                    // An unsuccessful request is delivered on the error channel.
                    observer.error(new HttpErrorResponse({
                        // The error in this case is the response body (error from the server).
                        error: body,
                        headers: headers,
                        status: status,
                        statusText: statusText,
                        url: url || undefined,
                    }));
                }
            };
            // The onError callback is called when something goes wrong at the network level.
            // Connection timeout, DNS error, offline, etc. These are actual errors, and are
            // transmitted on the error channel.
            var /** @type {?} */ onError = function (error) {
                var /** @type {?} */ res = new HttpErrorResponse({
                    error: error,
                    status: xhr.status || 0,
                    statusText: xhr.statusText || 'Unknown Error',
                });
                observer.error(res);
            };
            // The sentHeaders flag tracks whether the HttpResponseHeaders event
            // has been sent on the stream. This is necessary to track if progress
            // is enabled since the event will be sent on only the first download
            // progerss event.
            var /** @type {?} */ sentHeaders = false;
            // The download progress event handler, which is only registered if
            // progress events are enabled.
            var /** @type {?} */ onDownProgress = function (event) {
                // Send the HttpResponseHeaders event if it hasn't been sent already.
                if (!sentHeaders) {
                    observer.next(partialFromXhr());
                    sentHeaders = true;
                }
                // Start building the download progress event to deliver on the response
                // event stream.
                var /** @type {?} */ progressEvent = {
                    type: HttpEventType.DownloadProgress,
                    loaded: event.loaded,
                };
                // Set the total number of bytes in the event if it's available.
                if (event.lengthComputable) {
                    progressEvent.total = event.total;
                }
                // If the request was for text content and a partial response is
                // available on XMLHttpRequest, include it in the progress event
                // to allow for streaming reads.
                if (req.responseType === 'text' && !!xhr.responseText) {
                    progressEvent.partialText = xhr.responseText;
                }
                // Finally, fire the event.
                observer.next(progressEvent);
            };
            // The upload progress event handler, which is only registered if
            // progress events are enabled.
            var /** @type {?} */ onUpProgress = function (event) {
                // Upload progress events are simpler. Begin building the progress
                // event.
                var /** @type {?} */ progress = {
                    type: HttpEventType.UploadProgress,
                    loaded: event.loaded,
                };
                // If the total number of bytes being uploaded is available, include
                // it.
                if (event.lengthComputable) {
                    progress.total = event.total;
                }
                // Send the event.
                observer.next(progress);
            };
            // By default, register for load and error events.
            xhr.addEventListener('load', onLoad);
            xhr.addEventListener('error', onError);
            // Progress events are only enabled if requested.
            if (req.reportProgress) {
                // Download progress is always enabled if requested.
                xhr.addEventListener('progress', onDownProgress);
                // Upload progress depends on whether there is a body to upload.
                if (reqBody !== null && xhr.upload) {
                    xhr.upload.addEventListener('progress', onUpProgress);
                }
            }
            // Fire the request, and notify the event stream that it was fired.
            xhr.send(reqBody);
            observer.next({ type: HttpEventType.Sent });
            // This is the return from the Observable function, which is the
            // request cancellation handler.
            return function () {
                // On a cancellation, remove all registered event listeners.
                xhr.removeEventListener('error', onError);
                xhr.removeEventListener('load', onLoad);
                if (req.reportProgress) {
                    xhr.removeEventListener('progress', onDownProgress);
                    if (reqBody !== null && xhr.upload) {
                        xhr.upload.removeEventListener('progress', onUpProgress);
                    }
                }
                // Finally, abort the in-flight request.
                xhr.abort();
            };
        });
    };
    return HttpXhrBackend;
}());
HttpXhrBackend.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"] },
];
/**
 * @nocollapse
 */
HttpXhrBackend.ctorParameters = function () { return [
    { type: XhrFactory, },
]; };
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
var XSRF_COOKIE_NAME = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["InjectionToken"]('XSRF_COOKIE_NAME');
var XSRF_HEADER_NAME = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["InjectionToken"]('XSRF_HEADER_NAME');
/**
 * Retrieves the current XSRF token to use with the next outgoing request.
 *
 * \@experimental
 * @abstract
 */
var HttpXsrfTokenExtractor = (function () {
    function HttpXsrfTokenExtractor() {
    }
    /**
     * Get the XSRF token to use with an outgoing request.
     *
     * Will be called for every request, so the token may change between requests.
     * @abstract
     * @return {?}
     */
    HttpXsrfTokenExtractor.prototype.getToken = function () { };
    return HttpXsrfTokenExtractor;
}());
/**
 * `HttpXsrfTokenExtractor` which retrieves the token from a cookie.
 */
var HttpXsrfCookieExtractor = (function () {
    /**
     * @param {?} doc
     * @param {?} platform
     * @param {?} cookieName
     */
    function HttpXsrfCookieExtractor(doc, platform, cookieName) {
        this.doc = doc;
        this.platform = platform;
        this.cookieName = cookieName;
        this.lastCookieString = '';
        this.lastToken = null;
        /**
         * \@internal for testing
         */
        this.parseCount = 0;
    }
    /**
     * @return {?}
     */
    HttpXsrfCookieExtractor.prototype.getToken = function () {
        if (this.platform === 'server') {
            return null;
        }
        var /** @type {?} */ cookieString = this.doc.cookie || '';
        if (cookieString !== this.lastCookieString) {
            this.parseCount++;
            this.lastToken = Object(__WEBPACK_IMPORTED_MODULE_6__angular_common__["ɵparseCookieValue"])(cookieString, this.cookieName);
            this.lastCookieString = cookieString;
        }
        return this.lastToken;
    };
    return HttpXsrfCookieExtractor;
}());
HttpXsrfCookieExtractor.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"] },
];
/**
 * @nocollapse
 */
HttpXsrfCookieExtractor.ctorParameters = function () { return [
    { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"], args: [__WEBPACK_IMPORTED_MODULE_6__angular_common__["DOCUMENT"],] },] },
    { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"], args: [__WEBPACK_IMPORTED_MODULE_1__angular_core__["PLATFORM_ID"],] },] },
    { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"], args: [XSRF_COOKIE_NAME,] },] },
]; };
/**
 * `HttpInterceptor` which adds an XSRF token to eligible outgoing requests.
 */
var HttpXsrfInterceptor = (function () {
    /**
     * @param {?} tokenService
     * @param {?} headerName
     */
    function HttpXsrfInterceptor(tokenService, headerName) {
        this.tokenService = tokenService;
        this.headerName = headerName;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    HttpXsrfInterceptor.prototype.intercept = function (req, next) {
        var /** @type {?} */ lcUrl = req.url.toLowerCase();
        // Skip both non-mutating requests and absolute URLs.
        // Non-mutating requests don't require a token, and absolute URLs require special handling
        // anyway as the cookie set
        // on our origin is not the same as the token expected by another origin.
        if (req.method === 'GET' || req.method === 'HEAD' || lcUrl.startsWith('http://') ||
            lcUrl.startsWith('https://')) {
            return next.handle(req);
        }
        var /** @type {?} */ token = this.tokenService.getToken();
        // Be careful not to overwrite an existing header of the same name.
        if (token !== null && !req.headers.has(this.headerName)) {
            req = req.clone({ headers: req.headers.set(this.headerName, token) });
        }
        return next.handle(req);
    };
    return HttpXsrfInterceptor;
}());
HttpXsrfInterceptor.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"] },
];
/**
 * @nocollapse
 */
HttpXsrfInterceptor.ctorParameters = function () { return [
    { type: HttpXsrfTokenExtractor, },
    { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"], args: [XSRF_HEADER_NAME,] },] },
]; };
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Constructs an `HttpHandler` that applies a bunch of `HttpInterceptor`s
 * to a request before passing it to the given `HttpBackend`.
 *
 * Meant to be used as a factory function within `HttpClientModule`.
 *
 * \@experimental
 * @param {?} backend
 * @param {?=} interceptors
 * @return {?}
 */
function interceptingHandler(backend, interceptors) {
    if (interceptors === void 0) { interceptors = []; }
    if (!interceptors) {
        return backend;
    }
    return interceptors.reduceRight(function (next, interceptor) { return new HttpInterceptorHandler(next, interceptor); }, backend);
}
/**
 * Factory function that determines where to store JSONP callbacks.
 *
 * Ordinarily JSONP callbacks are stored on the `window` object, but this may not exist
 * in test environments. In that case, callbacks are stored on an anonymous object instead.
 *
 * \@experimental
 * @return {?}
 */
function jsonpCallbackContext() {
    if (typeof window === 'object') {
        return window;
    }
    return {};
}
/**
 * `NgModule` which adds XSRF protection support to outgoing requests.
 *
 * Provided the server supports a cookie-based XSRF protection system, this
 * module can be used directly to configure XSRF protection with the correct
 * cookie and header names.
 *
 * If no such names are provided, the default is to use `X-XSRF-TOKEN` for
 * the header name and `XSRF-TOKEN` for the cookie name.
 *
 * \@experimental
 */
var HttpClientXsrfModule = (function () {
    function HttpClientXsrfModule() {
    }
    /**
     * Disable the default XSRF protection.
     * @return {?}
     */
    HttpClientXsrfModule.disable = function () {
        return {
            ngModule: HttpClientXsrfModule,
            providers: [
                { provide: HttpXsrfInterceptor, useClass: NoopInterceptor },
            ],
        };
    };
    /**
     * Configure XSRF protection to use the given cookie name or header name,
     * or the default names (as described above) if not provided.
     * @param {?=} options
     * @return {?}
     */
    HttpClientXsrfModule.withOptions = function (options) {
        if (options === void 0) { options = {}; }
        return {
            ngModule: HttpClientXsrfModule,
            providers: [
                options.cookieName ? { provide: XSRF_COOKIE_NAME, useValue: options.cookieName } : [],
                options.headerName ? { provide: XSRF_HEADER_NAME, useValue: options.headerName } : [],
            ],
        };
    };
    return HttpClientXsrfModule;
}());
HttpClientXsrfModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"], args: [{
                providers: [
                    HttpXsrfInterceptor,
                    { provide: HTTP_INTERCEPTORS, useExisting: HttpXsrfInterceptor, multi: true },
                    { provide: HttpXsrfTokenExtractor, useClass: HttpXsrfCookieExtractor },
                    { provide: XSRF_COOKIE_NAME, useValue: 'XSRF-TOKEN' },
                    { provide: XSRF_HEADER_NAME, useValue: 'X-XSRF-TOKEN' },
                ],
            },] },
];
/**
 * @nocollapse
 */
HttpClientXsrfModule.ctorParameters = function () { return []; };
/**
 * `NgModule` which provides the `HttpClient` and associated services.
 *
 * Interceptors can be added to the chain behind `HttpClient` by binding them
 * to the multiprovider for `HTTP_INTERCEPTORS`.
 *
 * \@experimental
 */
var HttpClientModule = (function () {
    function HttpClientModule() {
    }
    return HttpClientModule;
}());
HttpClientModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"], args: [{
                imports: [
                    HttpClientXsrfModule.withOptions({
                        cookieName: 'XSRF-TOKEN',
                        headerName: 'X-XSRF-TOKEN',
                    }),
                ],
                providers: [
                    HttpClient,
                    // HttpHandler is the backend + interceptors and is constructed
                    // using the interceptingHandler factory function.
                    {
                        provide: HttpHandler,
                        useFactory: interceptingHandler,
                        deps: [HttpBackend, [new __WEBPACK_IMPORTED_MODULE_1__angular_core__["Optional"](), new __WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"](HTTP_INTERCEPTORS)]],
                    },
                    HttpXhrBackend,
                    { provide: HttpBackend, useExisting: HttpXhrBackend },
                    BrowserXhr,
                    { provide: XhrFactory, useExisting: BrowserXhr },
                ],
            },] },
];
/**
 * @nocollapse
 */
HttpClientModule.ctorParameters = function () { return []; };
/**
 * `NgModule` which enables JSONP support in `HttpClient`.
 *
 * Without this module, Jsonp requests will reach the backend
 * with method JSONP, where they'll be rejected.
 *
 * \@experimental
 */
var HttpClientJsonpModule = (function () {
    function HttpClientJsonpModule() {
    }
    return HttpClientJsonpModule;
}());
HttpClientJsonpModule.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"], args: [{
                providers: [
                    JsonpClientBackend,
                    { provide: JsonpCallbackContext, useFactory: jsonpCallbackContext },
                    { provide: HTTP_INTERCEPTORS, useClass: JsonpInterceptor, multi: true },
                ],
            },] },
];
/**
 * @nocollapse
 */
HttpClientJsonpModule.ctorParameters = function () { return []; };
/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Generated bundle index. Do not edit.
 */

//# sourceMappingURL=http.es5.js.map


/***/ }),

/***/ 869:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateAssetModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_asset_modal_service__ = __webpack_require__(872);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__create_asset_modal__ = __webpack_require__(873);




var CreateAssetModalComponent = /** @class */ (function () {
    function CreateAssetModalComponent(createAssetModalService) {
        this.createAssetModalService = createAssetModalService;
        this.model = new __WEBPACK_IMPORTED_MODULE_3__create_asset_modal__["a" /* CreateAssetModel */]();
    }
    CreateAssetModalComponent.prototype.show = function () {
        this.createAssetModal.show();
        this.getQuantity();
        this.getCustomers();
    };
    CreateAssetModalComponent.prototype.hide = function () {
        this.createAssetModal.hide();
    };
    CreateAssetModalComponent.prototype.getQuantity = function () {
        this.model.quantityList = Array.from(Array(100).keys());
    };
    CreateAssetModalComponent.prototype.getCustomers = function () {
        var _this = this;
        var tenantId = JSON.parse(localStorage.getItem('profile'));
        this.createAssetModalService.getCustomers(tenantId.tenantId)
            .subscribe(function (data) {
            _this.model.customerList = data;
        }, function (error) {
            console.log(error);
        });
    };
    CreateAssetModalComponent.prototype.getLocations = function (tenantId) {
        var _this = this;
        this.createAssetModalService.getLocations(tenantId)
            .subscribe(function (data) {
            _this.model.locationList = data;
        }, function (error) {
            console.log(error);
        });
    };
    CreateAssetModalComponent.prototype.getMarketSegments = function (tenantId) {
        var _this = this;
        this.createAssetModalService.getMarketSegments(tenantId)
            .subscribe(function (data) {
            _this.model.marketSegment = data;
        }, function (error) {
            console.log(error);
        });
    };
    CreateAssetModalComponent.prototype.customerChange = function (tenantId) {
        this.getLocations(tenantId);
        this.getMarketSegments(tenantId);
    };
    CreateAssetModalComponent.prototype.selectHose = function () {
        var _this = this;
        this.createAssetModalService.getComponentPart('Raw Hose', true)
            .subscribe(function (data) {
            _this.model.hoseList = data;
            console.log("this.model.hoseList", _this.model.hoseList);
        }, function (error) {
            console.log(error);
        });
    };
    CreateAssetModalComponent.prototype.getPartDetail = function (hose) {
        var _this = this;
        console.log("hose", hose);
        this.createAssetModalService.getComponentPartbyId(hose)
            .subscribe(function (data) {
            _this.model.componentPartbyIdList = data;
            console.log("this.model.componentPartbyIdList", _this.model.componentPartbyIdList);
        }, function (error) {
            console.log(error);
        }, function () {
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('createAssetModal'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["a" /* ModalDirective */])
    ], CreateAssetModalComponent.prototype, "createAssetModal", void 0);
    CreateAssetModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-create-asset-modal',
            template: __webpack_require__(874),
            styles: [__webpack_require__(875)],
            providers: [__WEBPACK_IMPORTED_MODULE_2__create_asset_modal_service__["a" /* CreateAssetModalService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__create_asset_modal_service__["a" /* CreateAssetModalService */]])
    ], CreateAssetModalComponent);
    return CreateAssetModalComponent;
}());



/***/ }),

/***/ 870:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(110);


var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
        this.cachedRequests = [];
    }
    AuthGuard.prototype.getToken = function () {
        return localStorage.getItem('token');
    };
    AuthGuard.prototype.canActivate = function () {
        if (localStorage.getItem('token')) {
            return true;
        }
        else {
            this.router.navigate(['/']);
            return false;
        }
    };
    AuthGuard.prototype.collectFailedRequest = function (request) {
        this.cachedRequests.push(request);
    };
    AuthGuard.prototype.retryFailedRequests = function () {
        // retry the requests. this method can
        // be called after the token is refreshed
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ 871:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ErrorModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__(189);


var ErrorModalComponent = /** @class */ (function () {
    function ErrorModalComponent() {
    }
    ErrorModalComponent.prototype.show = function () {
        this.errorModal.show();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('errorModal'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["a" /* ModalDirective */])
    ], ErrorModalComponent.prototype, "errorModal", void 0);
    ErrorModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-error-modal',
            template: __webpack_require__(878),
            styles: [__webpack_require__(879)]
        }),
        __metadata("design:paramtypes", [])
    ], ErrorModalComponent);
    return ErrorModalComponent;
}());



/***/ }),

/***/ 872:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateAssetModalService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(868);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_global__ = __webpack_require__(335);




var CreateAssetModalService = /** @class */ (function () {
    function CreateAssetModalService(http) {
        this.http = http;
    }
    CreateAssetModalService.prototype.getCustomers = function (tenantId) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__app_global__["b" /* baseUrl */] + 'customer/list/' + tenantId);
    };
    CreateAssetModalService.prototype.getLocations = function (tenantId) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__app_global__["b" /* baseUrl */] + 'location/lookup/list/' + tenantId);
    };
    CreateAssetModalService.prototype.getMarketSegments = function (tenantId) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__app_global__["b" /* baseUrl */] + 'getCustomerMarketSegmentApplication/' + tenantId);
    };
    CreateAssetModalService.prototype.getComponentPart = function (partType, isAll) {
        var obj = {
            'ComponentPartType': partType,
            'IsAll': isAll
        };
        var body = JSON.stringify(obj);
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__app_global__["b" /* baseUrl */] + 'componentPart/list', body);
    };
    CreateAssetModalService.prototype.getComponentPartbyId = function (id) {
        var obj = {
            'ComponentPartId': id
        };
        var body = JSON.stringify(obj);
        return this.http.post(__WEBPACK_IMPORTED_MODULE_3__app_global__["b" /* baseUrl */] + 'componentPart/GetComponentPartById', body);
    };
    CreateAssetModalService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], CreateAssetModalService);
    return CreateAssetModalService;
}());



/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateAssetModel; });
var CreateAssetModel = /** @class */ (function () {
    function CreateAssetModel() {
        this.customerList = [];
        this.quantityList = [];
        this.locationList = [];
        this.hoseList = [];
        this.componentPartbyIdList = [];
    }
    return CreateAssetModel;
}());



/***/ }),

/***/ 874:
/***/ (function(module, exports) {

module.exports = "<div bsModal #createAssetModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\" size=\"lg\">\r\n  <div class=\"modal-dialog modal-large\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <button (click)=\"createAssetModal.hide()\" aria-label=\"Close\" class=\"close\" type=\"button\">\r\n          <span aria-hidden=\"true\">×</span>\r\n        </button>\r\n        <h5 class=\"modal-title text-center\" id=\"myModalLabel18\">Create Asset</h5>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <ul class=\"nav nav-tabs float-xs-left\" id=\"myTab\" role=\"tablist\">\r\n          <li class=\"nav-item active\">\r\n            <a class=\"nav-link\" id=\"customer-tab\" data-toggle=\"tab\" data-target=\"#customerTab\" role=\"tab\" aria-controls=\"customer-modal-tab\" aria-expanded=\"true\">Customer</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" id=\"hose-tab\" data-toggle=\"tab\" data-target=\"#hoseTab\" role=\"tab\" aria-controls=\"hose-modal-tab\" aria-expanded=\"false\">Hose</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link active\" id=\"fitting1-tab\" data-toggle=\"tab\" data-target=\"#fitting1Tab\" role=\"tab\" aria-controls=\"fitting1-modal-tab\" aria-expanded=\"true\">Fitting 1</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link active\" id=\"fitting2-tab\" data-toggle=\"tab\" data-target=\"#fitting2Tab\" role=\"tab\" aria-controls=\"fitting2-modal-tab\" aria-expanded=\"true\">Fitting 2</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link active\" id=\"save-print-tab\" data-toggle=\"tab\" data-target=\"#savePrintTab\" role=\"tab\" aria-controls=\"save-print-modal-tab\" aria-expanded=\"true\">Save & Print</a>\r\n          </li>\r\n        </ul>\r\n        <form #form=\"ngForm\">\r\n          <div class=\"tab-content mb-lg\" id=\"myTabContent\">\r\n            <div role=\"tabpanel\" class=\"tab-pane active in clearfix\" id=\"customerTab\" aria-labelledby=\"customer-modal-tab\" aria-expanded=\"true\">\r\n              <section class=\"widget\" widget>\r\n                <div class=\"widget-body eaton-pt-10 eaton-pb-40\">\r\n                  <span class=\"pull-right\">\r\n                    <button class=\"btn btn-primary btn-sm\" (click)=\"selectHose()\">Next</button>\r\n                  </span>\r\n                  <div class=\"eaton-pt-50 eaton-pb-50\">\r\n                    <div class=\"form-group eaton-form\">\r\n                      <label for=\"select-quantity\">Quantity</label>\r\n                      <select name=\"select-quantity\" id=\"select-quantity\" class=\"form-control\" required [(ngModel)]=\"quantity\" [ngModelOptions]=\"{standalone: true}\">\r\n                        <option *ngFor=\"let quan of model.quantityList\" [value]=\"quan\">{{quan}}</option>\r\n                      </select>\r\n                    </div>\r\n                    <div class=\"form-group eaton-form\">\r\n                      <label for=\"select-customer\">Customer</label>\r\n                      <select name=\"select-customer\" id=\"select-customer\" class=\"form-control\" [ngModel]=\"customer\" (ngModelChange)=\"customerChange($event)\" required>\r\n                        <option *ngFor=\"let cust of model.customerList\" [value]=\"cust._id\">{{cust.tenantName}}</option>\r\n                      </select>\r\n                    </div>\r\n                    <div class=\"form-group eaton-form\">\r\n                      <label for=\"select-location\">Location</label>\r\n                      <select name=\"select-location\" id=\"select-location\" class=\"form-control\" required [(ngModel)]=\"location\" [ngModelOptions]=\"{standalone: true}\">\r\n                        <option *ngFor=\"let loc of model.locationList\" [value]=\"loc.id\">{{loc.name}}</option>\r\n                      </select>\r\n                    </div>\r\n                    <div class=\"form-group eaton-form\">\r\n                      <label for=\"select-customer\">Market Segment</label>\r\n                      <input type=\"text\" class=\"form-control\" placeholder=\"Market Segment\" [value]=\"model.marketSegment?.marketSegmentName\" readonly />\r\n                    </div>\r\n                    <div class=\"form-group eaton-form\">\r\n                      <label for=\"select-application\">Applicatiton</label>\r\n                      <select name=\"select-application\" id=\"select-application\" class=\"form-control\" required [(ngModel)]=\"application\" [ngModelOptions]=\"{standalone: true}\">\r\n                        <option *ngFor=\"let app of model.marketSegment?.applications\" [value]=\"app.applicationDescription\">{{app.applicationName}}</option>\r\n                      </select>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"text-center\" *ngIf=\"model.customerList.length == 0 && model.locationList.length == 0\"><i class=\"icon-sunrays\"></i> Loading ... </div>\r\n                </div>\r\n              </section>\r\n            </div>\r\n            <div class=\"tab-pane\" id=\"hoseTab\" role=\"tabpanel\" aria-labelledby=\"hose-modal-tab\" aria-expanded=\"false\">\r\n              <section class=\"widget\" widget>\r\n                <div class=\"widget-body\">\r\n                  <span class=\"pull-right eaton-pt-10 eaton-pb-10\">\r\n                    <a class=\"btn btn-primary btn-sm\">Back</a>\r\n                    <a class=\"btn btn-primary btn-sm\">Next</a>\r\n                  </span>\r\n                  <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                    <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 hoselist\">\r\n                      <div class=\"text-center\" *ngIf=\"model.hoseList?.componentPartList?.length == 0\"><i class=\"icon-sunrays\"></i> Loading ... </div>\r\n                      <div *ngFor=\"let hose of model.hoseList.componentPartList\">\r\n                        <label>\r\n                          <input type=\"radio\" name=\"hoseRadio\" [value]=\"hose._id\" (click)=\"getPartDetail(hose._id)\" />{{hose.componentPartNumber}}\r\n                        </label>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"col-lg-9\" style=\"display:grid;\">\r\n                      <div class=\"panel-group\" id=\"accordion\">\r\n                        <div class=\"panel panel-default\">\r\n                          <div class=\"panel-heading\">\r\n                            <h4 class=\"panel-title\">\r\n                              <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">\r\n                                Hose Detail\r\n                              </a>\r\n                            </h4>\r\n                          </div>\r\n                          <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\r\n                            <div class=\"panel-body\">\r\n                              <div class=\"col-md-6\">\r\n                                <div class=\"form-group\">\r\n                                  <label for=\"overall-length\">Market Segment</label>\r\n                                  <input type=\"text\" class=\"form-control\" placeholder=\"Overall Length\" autofocus />\r\n                                </div>\r\n                                <div class=\"form-group\">\r\n                                  <label for=\"overall-length\">Market Segment</label>\r\n                                  <input type=\"text\" class=\"form-control\" [value]=\"model.componentPartbyIdList?.componentPartNumber\" readonly />\r\n                                </div>\r\n                                <div class=\"form-group\">\r\n                                  <label for=\"desc\">Market Segment</label>\r\n                                  <input type=\"text\" class=\"form-control\" [value]=\"model.componentPartbyIdList?.description\" />\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"col-md-6\">\r\n                                <img src=\"https://static.grainger.com/rp/s/is/image/Grainger/6DPL9_AS01?$mdmain$\" width=\"250\" />\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"panel panel-default\">\r\n                          <div class=\"panel-heading\">\r\n                            <h4 class=\"panel-title\">\r\n                              <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwo\">\r\n                                Hose Properties\r\n                              </a>\r\n                            </h4>\r\n                          </div>\r\n                          <div id=\"collapseTwo\" class=\"panel-collapse collapse\">\r\n                            <div class=\"panel-body\">\r\n                              <div>\r\n                                <div class=\"form-group eaton-form\" *ngFor=\"let hoseDetail of model.componentPartbyIdList?.componentPartDetails\">\r\n                                  <label for=\"overall-length\">{{hoseDetail.propertyName}}</label>\r\n                                  <input type=\"{{hoseDetail.propertyType}}\" class=\"form-control\" [value]=\"hoseDetail.propertyValue\" readonly />\r\n                                </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </section>\r\n            </div>\r\n            <div class=\"tab-pane\" id=\"fitting1Tab\" role=\"tabpanel\" aria-labelledby=\"fitting1-modal-tab\" aria-expanded=\"false\">\r\n              <section class=\"widget\" widget>\r\n                <div class=\"widget-body\">\r\n                  <span class=\"pull-right\">\r\n                    <a class=\"btn btn-primary btn-sm\">Back</a>\r\n                    <a class=\"btn btn-primary btn-sm\">Next</a>\r\n                  </span>\r\n                  fitting 1 tab\r\n                </div>\r\n              </section>\r\n            </div>\r\n            <div class=\"tab-pane\" id=\"fitting2Tab\" role=\"tabpanel\" aria-labelledby=\"fitting2-modal-tab\" aria-expanded=\"false\">\r\n              <section class=\"widget\" widget>\r\n                <div class=\"widget-body\">\r\n                  <span class=\"pull-right\">\r\n                    <a class=\"btn btn-primary btn-sm\">Back</a>\r\n                    <a class=\"btn btn-primary btn-sm\">Next</a>\r\n                  </span>\r\n                  fitting 2 tab\r\n                </div>\r\n              </section>\r\n            </div>\r\n            <div class=\"tab-pane\" id=\"savePrintTab\" role=\"tabpanel\" aria-labelledby=\"save-print-modal-tab\" aria-expanded=\"false\">\r\n              <section class=\"widget\" widget>\r\n                <div class=\"widget-body\">\r\n                  <span class=\"pull-right\">\r\n                    <a class=\"btn btn-primary btn-sm\">Back</a>\r\n                    <a class=\"btn btn-primary btn-sm\">Save & Print</a>\r\n                  </span>\r\n                  save and print\r\n                </div>\r\n              </section>\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <div class=\"col-lg-12 col-xs-12 text-center\">\r\n          <i class=\"fa fa-circle-o-notch fa-spin\"></i>\r\n        </div>\r\n        <button class=\"btn btn-primary btn-sm\" (click)=\"createAssetModal.hide()\">Cancel</button>\r\n        <button class=\"btn btn-primary btn-sm\" (click)=\"createAsset()\">Save</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ 875:
/***/ (function(module, exports, __webpack_require__) {


        var result = __webpack_require__(876);

        if (typeof result === "string") {
            module.exports = result;
        } else {
            module.exports = result.toString();
        }
    

/***/ }),

/***/ 876:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(63)(undefined);
// imports


// module
exports.push([module.i, "/*.nav-item{\r\n  pointer-events: none;\r\n}*/\r\n\r\n.eaton-form{\r\n  width: 30%;\r\n  display: inline-block;\r\n  margin-right: 10px;\r\n}\r\n\r\n.hoselist {\r\n  max-height:500px;\r\n  overflow: auto;\r\n}\r\n\r\n.widget{\r\n  display: grid;\r\n}\r\n", ""]);

// exports


/***/ }),

/***/ 877:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TokenInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(868);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authGuard_auth_guard_service__ = __webpack_require__(870);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modals_error_modal_error_modal_component__ = __webpack_require__(871);




var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor(auth) {
        this.auth = auth;
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        request = request.clone({
            setHeaders: {
                'Authorization': "Bearer " + this.auth.getToken(),
                'Content-Type': 'application/json'
            }
        });
        return next.handle(request).do(function (event) {
            if (event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["e" /* HttpResponse */]) {
                // do stuff with response if you want
            }
        }, function (err) {
            if (err instanceof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpErrorResponse */]) {
                console.log("error", err);
                //this.errorModal.show();
                //if (err.status === 405) {
                //    // redirect to the login route
                //    // or show a modal
                //    alert("something went wrong from token interceptor");
                //    this.auth.collectFailedRequest(request);
                //}
            }
        });
        ;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('errorModal'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__modals_error_modal_error_modal_component__["a" /* ErrorModalComponent */])
    ], TokenInterceptor.prototype, "errorModal", void 0);
    TokenInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__authGuard_auth_guard_service__["a" /* AuthGuard */]])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ 878:
/***/ (function(module, exports) {

module.exports = "<!--Error Window Modal-->\r\n<div bsModal #errorModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\">\r\n    <form>\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n          <button (click)=\"errorModal.hide()\" aria-label=\"Close\" class=\"close\" type=\"button\">\r\n            <span aria-hidden=\"true\">×</span>\r\n          </button>\r\n          <h5 class=\"modal-title text-center\" id=\"myModalLabel18\">Oops!! Looks like something went wrong</h5>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          test\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n          <button class=\"btn btn-primary btn-sm\" (click)=\"errorModal.hide()\">Close</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ 879:
/***/ (function(module, exports, __webpack_require__) {


        var result = __webpack_require__(880);

        if (typeof result === "string") {
            module.exports = result;
        } else {
            module.exports = result.toString();
        }
    

/***/ }),

/***/ 880:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(63)(undefined);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/***/ }),

/***/ 881:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModel; });
var AppModel = /** @class */ (function () {
    function AppModel() {
        this.isLoggedIn = false;
    }
    return AppModel;
}());



/***/ })

})
//# sourceMappingURL=main.map